import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import ProductList from './ProductScreen/ProductList';
import ProductDetails from './ProductScreen/ProductDetails';
import AddProduct from './ProductScreen/AddProduct'
import editProduct from './ProductScreen/editProduct';
import ProductGallery from './ProductScreen/ProductGallery';

const MainNavigator = createStackNavigator({
    ProductList: {
        screen: ProductList,
        navigationOptions: ({navigation}) => ({
            title : 'Products',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        })
    },
    ProductDetails: {
        screen: ProductDetails,
    },
    AddProduct: {
        screen: AddProduct,
    },
    editProduct: {
        screen: editProduct,
    },
    ProductGallery: {
        screen: ProductGallery,
    }
});

export default createAppContainer(MainNavigator);