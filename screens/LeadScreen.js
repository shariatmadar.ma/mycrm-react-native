import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import LeadList from './LeadScreen/LeadList';
import LeadDetails from './LeadScreen/LeadDetails';
import AddLead from './LeadScreen/AddLead';
import LeadAttachments from './LeadScreen/LeadAttachments';
import LeadTasks from './LeadScreen/LeadTasks';
import LeadProfileDetails from './LeadScreen/LeadProfileDetails';
import LeadProduct from './LeadScreen/LeadProduct';
import editLead from './LeadScreen/editLead';
import LeadCalls from './LeadCalls';
import LeadSMS from './LeadScreen/LeadSMS'

const MainNavigator = createStackNavigator({
    LeadList: {
        screen: LeadList,
        navigationOptions: ({navigation}) => ({
            title : 'Leads',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        })
    },
    LeadDetails: {
        screen: LeadDetails,
    },
    AddLead: {
        screen: AddLead,
    },
    LeadAttachments: {
        screen: LeadAttachments,
    },
    LeadTasks: {
        screen: LeadTasks,
    },
    LeadProfileDetails: {
        screen: LeadProfileDetails,
    },
    LeadProduct: {
        screen: LeadProduct,
    },
    editLead: {
        screen: editLead,
    },
    LeadCalls: {
        screen: LeadCalls
    },
    LeadSMS: {
        screen: LeadSMS
    }
});

export default createAppContainer(MainNavigator);