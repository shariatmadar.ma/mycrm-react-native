import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableWithoutFeedback, Animated, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import SQLite from 'react-native-sqlite-storage';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import LottieView from 'lottie-react-native';
import { Avatar, CheckBox } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class Dashboard extends Component {

    static navigationOptions = ({navigation}) => ({
        title : null,
        headerTransparent: true,
        headerStyle: {
        },
    });

    constructor (props) {
        super(props);
        var day = new Date().getDate();
        if (day < 10) {
            day = "0" + day;
        }
        var month = new Date().getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }
        var year = new Date().getFullYear();
        this.state = {
            name: '',
            family: '',
            job: '',
            pic: '',
            mobile: '',
            phone: '',
            email: '',
            company: '',
            loadingVisible: false,
            isLoading: true,
            animation: new Animated.Value(0),
            active: false,
            date: year + "-" + month + "-" + day,
            day: new Date().getDate(),
            monthInText: "",
            monthInNumber: new Date().getMonth() + 1,
            todayTasks: [],
            todayTasksNumber: 0,
            doneTasksNumber: 0,
            currentIndex: 0,
            checked: null,
            leadsNumber: 0,
            productsNumber: 0,
        }
    }

    _userData = async() => {
        try {
            const name = await AsyncStorage.getItem('name')
            const family = await AsyncStorage.getItem('family')
            const job = await AsyncStorage.getItem('job')
            const mobile = await AsyncStorage.getItem('mobile')
            const pic = await AsyncStorage.getItem('pic')
            const phone = await AsyncStorage.getItem('phone')
            const email = await AsyncStorage.getItem('email')
            const company = await AsyncStorage.getItem('company')
            this.setState({
                name,
                family,
                job,
                mobile,
                pic,
                phone,
                email,
                company
            })
        } catch (e) {
            console.log(e)
        }
    }

    componentDidMount () {
        this._userData();
        SQLite.enablePromise(false);
        let db = SQLite.openDatabase(
            {
                name: 'CRMDatabase.db', 
                createFromLocation : "~database.db", 
                location: 'Library'
            }, 
            this.openCB, 
            this.errorCB
        );
        db.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS Leads (' +
                'ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
                'Name TEXT NOT NULL,' +
                'Family TEXT NOT NULL,' +
                'Phone TEXT NOT NULL,' +
                'Company TEXT,' +
                'Email TEXT,' +
                'Mobile TEXT NOT NULL,' +
                'Street TEXT,' +
                'City TEXT,' +
                'State TEXT,' +
                'ZipCode NUMERIC,' +
                'Country TEXT,' +
                'Desc TEXT,' +
                'Lat NUMERIC,' +
                'Lng NUMERIC,' +
                'Image Text)', [],    
            );
            console.log("Leads Table Created.");
        });
        db.transaction(tx => {
            tx.executeSql( 'CREATE TABLE IF NOT EXISTS ProductImage (ProductID INTEGER NOT NULL, Image TEXT)', [] );
            console.log("ProductImage Table Created");
        })
        db.transaction(tx => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS LeadAttachments (' +
            'LeadID INTEGER NOT NULL,' +
            'fileName TEXT,' +
            'fileType TEXT,' +
            'filePath TEXT)', [],)
            console.log("LeadAttachments Table Created");
        })
        db.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS Products ( ' +
            'ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
            'Code INTEGER NOT NULL,' +
            'Name TEXT NOT NULL,' +
            'Activity BOOLEAN,' +
            'Price INTEGER,' +
            'Desc TEXT,'+
            'Image TEXT)', [],    
            );
            console.log("Products Table Created.");
        });
        db.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS Tasks (' +
            'ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,' +
            'Subject TEXT NOT NULL,' +
            'Date TEXT NOT NULL,' +
            'Status ENUM["Complete", "Incomplete"],' +
            'Priority ENUM["high", "normal", "low"],' +
            'Desc TEXT,' +
            'Lead TEXT)', [],    
            );
            console.log("Tasks Table Created.");
        });
        db.transaction((tx) =>{
            tx.executeSql('CREATE TABLE IF NOT EXISTS LeadProducts (' +
            'LeadID INTEGER NOT NULL,' + 'ProductID INTEGER NOT NULL)', []
            );
            console.log("LeadProducts Table Created");
        })
        
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            db.transaction(tx => {
                tx.executeSql('SELECT * FROM Leads', [], (tx, result) => {
                    console.log("Query completed");
                    var len = result.rows.length;
                    this.setState({
                        isLoading: false,
                        leadsNumber: len,
                    })
                })
            });
            db.transaction(tx => {
                tx.executeSql('SELECT * FROM Products', [], (tx, result) => {
                    console.log("Query completed");
                    var len = result.rows.length;
                    this.setState({
                        isLoading: false,
                        productsNumber: len,
                    })
                })
            });
            db.transaction(tx => {
                tx.executeSql('SELECT ID, Subject, Status, Priority, Lead FROM Tasks WHERE Date LIKE ?', [this.state.date], (tx, result) => {
                    console.log("Query completed");
                    var data = [];
                    var len = result.rows.length;
                    var selectedItems = new Array();
                    console.log("TsakLen:" + len);
                    for (let i = 0; i < len; i++) {
                        let row = result.rows.item(i);
                        data.push(row);
                        if (result.rows.item(i).Status == "Complete") {
                            selectedItems.push(true)
                        } else {
                            selectedItems.push(false)
                        }
                    }
                    console.log(data)
                    console.log(selectedItems)
                    this.setState({
                        todayTasks: data,
                        checked: selectedItems,
                        isLoading: false,
                    })
                    // var selectedItems = new Array(this.state.products.length);
                    // selectedItems.fill(false);
                    // this.setState({
                        // checked: selectedItems,
                    console.log("checked:" +  this.state.checked)
                })
            });
            db.transaction(tx => {
                tx.executeSql('SELECT * FROM Tasks WHERE Date LIKE ?', [this.state.date], (tx, result) => {
                    console.log("Query completed");
                    var len = result.rows.length;
                    this.setState({
                        todayTasksNumber: len,
                        isLoading: false,
                    })
                })
            });
            db.transaction(tx => {
                tx.executeSql('SELECT * FROM Tasks WHERE Date LIKE ? AND Status LIKE ?', [this.state.date, "Complete"], (tx, result) => {
                    console.log("Query completed");
                    var len = result.rows.length;
                    this.setState({
                        isLoading: false,
                        doneTasksNumber: len
                    })
                })
            });
        });

        if (this.state.monthInNumber == 1) {
            this.setState({
                monthInText: "Jan"
            })
        } else if (this.state.monthInNumber == 2) {
            this.setState({
                monthInText: "Feb"
            })
        } else if (this.state.monthInNumber == 3) {
            this.setState({
                monthInText: "Mar"
            })
        } else if (this.state.monthInNumber == 4) {
            this.setState({
                monthInText: "Apr"
            })
        } else if (this.state.monthInNumber == 5) {
            this.setState({
                monthInText: "May"
            })
        } else if (this.state.monthInNumber == 6) {
            this.setState({
                monthInText: "Jun"
            })
        } else if (this.state.monthInNumber == 7) {
            this.setState({
                monthInText: "Jul"
            })
        } else if (this.state.monthInNumber == 8) {
            this.setState({
                monthInText: "Aug"
            })
        } else if (this.state.monthInNumber == 9) {
            this.setState({
                monthInText: "Sep"
            })
        } else if (this.state.monthInNumber == 10) {
            this.setState({
                monthInText: "Oct"
            })
        } else if (this.state.monthInNumber == 11) {
            this.setState({
                monthInText: "Nov"
            })
        } else if (this.state.monthInNumber == 12) {
            this.setState({
                monthInText: "Dec"
            })
        }
    }

    toggleOpen = () => {
        const toValue = this._open ? 0 : 1;
        Animated.timing(this.state.animation, {
            toValue,
            duration: 200
        }).start();
        this._open = !this._open;
    }
    
    openCB = () => {
        console.log("Database Opened.");
    }
    errorCB = (err) => {
        console.log("Error: " + err); 
    }

    _renderItem ({item, index}) {
        return (
            <View>
                <Text>{ item.Subject }</Text>
                <Image style = {{width: 70, height: 100}} source = {require('../pics/me.jpg')}/>
            </View>
        );
    }

    updateTaskTable = (task) => {
        var db = SQLite.openDatabase({
            name: "CRMDatabase.db",
            createFromLocation: "~database.db",
            location: "Library"
        })
        db.transaction(tx =>{
            tx.executeSql('UPDATE Tasks SET Status = ? WHERE ID = ?', [task.Status, task.ID])
        })
    }

    render () {

        const bgStyle = {
            transform: [{
                scale: this.state.animation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 30]
                })
            }]
        }

        const taskPlusStyle = {
            transform: [{
                scale: this.state.animation
            }, 
            {
                translateY: this.state.animation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0 , -60]
                })
            }]
        } 

        const userPlusStyle = {
            transform: [{
                scale: this.state.animation
            }, 
            {
                translateY: this.state.animation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0 , -110]
                })
            }]
        }

        const productPlusStyle = {
            transform: [{
                scale: this.state.animation
            }, 
            {
                translateY: this.state.animation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0 , -160]
                })
            }]
        }

        const labelPositionInterpolate = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 150]
        })

        const opacityInterpolate = this.state.animation.interpolate({
            inputRange: [0, .8, 1],
            outputRange: [0, 0, 1]
        })

        const labelStyle = {
            opacity: opacityInterpolate,
            transform: [{
                translateX: labelPositionInterpolate
            }]
        }

        if (this.state.todayTasks.length == 0) {
            return (
                <View style = {styles.container}>
                    <Animated.View style = {[styles.background, bgStyle]}/>
                    <View style = {styles.profileContainer}>
                        <Image 
                            style = {styles.avatar}
                            source = {(this.state.pic) ? {uri: this.state.pic} : null}                         
                        />   
                        <LottieView
                            style = {{width: '100%', position: 'absolute', top: -20, zIndex: 1, opacity: 0.95}} 
                            source={require('../lotties/lf30_editor_XTKZGS.json')} 
                            autoPlay 
                            loop
                        />   
                    </View>
                    <View style = {styles.detailContainer}>
                        <Avatar
                            source = {(this.state.pic) ? {uri: this.state.pic} : null}                        
                            rounded 
                            containerStyle = {styles.avatarrounded}
                            title = {"MA"}
                            size = {100}
                            showEditButton
                            onEditPress = {() => {this.props.navigation.navigate("OwnerProfile")}}
                        />
                        <Text style = {{fontSize: 22, fontWeight: 'bold', fontFamily: 'georgia', marginTop: 5, color: '#2d3258'}}>{this.state.name} {this.state.family}</Text>
                        <Text style = {{fontSize: 18, marginTop: 5, fontWeight: 'bold',fontFamily: 'georgia', color: '#2d3258'}}>{this.state.job}</Text>
                        <View style = {{flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, width: '100%', alignItems: 'center'}}>
                            <View style = {styles.number}>
                                <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#29490d'}}>{this.state.leadsNumber}</Text>
                                <Text style ={{fontSize: 16, color: '#29490d'}}>Leads</Text>
                            </View>
                            <View style ={{width: 1, height: '100%', backgroundColor: '#ddd'}}></View>
                            <View style = {styles.number}>
                                <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#29490d'}}>{this.state.productsNumber}</Text>
                                <Text style ={{fontSize: 16, color: '#29490d'}}>Products</Text>
                            </View>
                        </View>
                    </View>
                    <TouchableWithoutFeedback>
                        <Animated.View style = {[styles.other, userPlusStyle, styles.leadPlus]}>
                            <Animated.Text style = {[styles.label, labelStyle]}>{this.state.email}</Animated.Text>
                            <Icon name = "envelope" size = {20} color = {"#fff"}/>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback>
                        <Animated.View style = {[styles.other, taskPlusStyle, styles.taskPlus]}>
                            <Animated.Text style = {[styles.label, labelStyle]}>{this.state.company}</Animated.Text>
                            <Icon name = "building" size = {20} color = {"#ffff"}/>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback>
                        <Animated.View style = {[styles.other, productPlusStyle, styles.productPlus]}>
                            <Animated.Text style = {[styles.label, labelStyle]}>{this.state.mobile}</Animated.Text>
                            <Icon name = "phone" size = {20} color = {"#fff"}/>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress = {this.toggleOpen}>
                        <View style = {[styles.button]}>
                            <Icon name = "angle-double-up" size = {30} color = {"#fff"}/>
                        </View>
                    </TouchableWithoutFeedback>
                    <View style = {{marginTop: 45}}>
                        <Text style = {{fontSize: 20, fontWeight: 'bold', alignSelf: 'center', marginBottom: 10, color: '#2d3258', zIndex: 2}}>Today Tasks</Text>
                        <View style = {{
                            alignSelf: 'center',
                            // borderWidth: 2,
                            borderStyle: 'dashed',
                            alignItems: 'center', 
                            justifyContent: 'center',
                            padding : 10,
                            // backgroundColor: '',
                            borderRadius: 10
                        }}>
                            <Text style = {{fontSize: 18, color: '#2d3258'}}>Today there is nothing to do</Text>
                        </View>
                        <View style = {{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 10, paddingHorizontal: screenWidth / 7}}>
                            <View style ={{ width: 120, height: 120, borderRadius: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: '#29490d', marginVertical: 5}}>
                                <Text style = {{fontSize: 35, fontWeight: 'bold', color: '#e6f9ff'}}>{this.state.day}</Text>
                                <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#e6f9ff'}}>{this.state.monthInText}</Text>
                            </View>
                            <View style = {{alignItems: 'center', marginVertical: 5}}>
                                <AnimatedCircularProgress
                                    size={120}
                                    width={15}
                                    fill={(this.state.doneTasksNumber == 0) ? 0 : Math.round(this.state.doneTasksNumber / this.state.todayTasksNumber * 100)}
                                    tintColor="#27b46e"
                                    backgroundColor="#3d5875" 
                                >
                                { (fill) => ( <Text style = {{fontSize: 25, fontWeight: 'bold'}}> { Math.round(fill) } {"%"} </Text> ) }
                                </AnimatedCircularProgress>
                            </View>
                        </View>
                    </View>      
                </View>
            );
        }
        
        return (
            <View style = {styles.container}>
                <Animated.View style = {[styles.background, bgStyle]}/>
                <View style = {styles.profileContainer}>
                    <Image 
                        style = {styles.avatar}
                        source = {(this.state.pic) ? {uri: this.state.pic} : null}                         
                    />   
                    <LottieView
                        style = {{width: '100%', position: 'absolute', top: -20, zIndex: 1, opacity: 0.95}} 
                        source={require('../lotties/lf30_editor_XTKZGS.json')} 
                        autoPlay 
                        loop
                    />   
                </View>
                <View style = {styles.detailContainer}>
                    <Avatar
                        source = {(this.state.pic) ? {uri: this.state.pic} : null}                         
                        rounded 
                        containerStyle = {styles.avatarrounded}
                        title = {"MA"}
                        size = {100}
                        showEditButton
                        onEditPress = {() => {this.props.navigation.navigate("OwnerProfile")}}
                    />
                    <Text style = {{fontSize: 20, fontWeight: 'bold', fontFamily: 'georgia', marginTop: 5, color: '#2d3258'}}>{this.state.name} {this.state.family}</Text>
                    <Text style = {{fontSize: 18, marginTop: 5, fontFamily: 'georgia', color: '#2d3258'}}>{this.state.job}</Text>
                    <View style = {{flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, width: '100%', alignItems: 'center'}}>
                        <View style = {styles.number}>
                            <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#29490d'}}>{this.state.leadsNumber}</Text>
                            <Text style ={{fontSize: 16, color: '#29490d'}}>Leads</Text>
                        </View>
                        <View style ={{width: 1, height: '100%', backgroundColor: '#ddd'}}></View>
                        <View style = {styles.number}>
                            <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#29490d'}}>{this.state.productsNumber}</Text>
                            <Text style ={{fontSize: 16, color: '#29490d'}}>Products</Text>
                        </View>
                    </View>
                </View>
                <TouchableWithoutFeedback>
                    <Animated.View style = {[styles.other, userPlusStyle, styles.leadPlus]}>
                        <Animated.Text style = {[styles.label, labelStyle]}>{this.state.email}</Animated.Text>
                        <Icon name = "envelope" size = {20} color = {"#fff"}/>
                    </Animated.View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback>
                    <Animated.View style = {[styles.other, taskPlusStyle, styles.taskPlus]}>
                        <Animated.Text style = {[styles.label, labelStyle]}>{this.state.company}</Animated.Text>
                        <Icon name = "building" size = {20} color = {"#ffff"}/>
                    </Animated.View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback>
                    <Animated.View style = {[styles.other, productPlusStyle, styles.productPlus]}>
                        <Animated.Text style = {[styles.label, labelStyle]}>{this.state.mobile}</Animated.Text>
                        <Icon name = "phone" size = {20} color = {"#fff"}/>
                    </Animated.View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress = {this.toggleOpen}>
                    <View style = {[styles.button]}>
                        <Icon name = "angle-double-up" size = {30} color = {"#fff"}/>
                    </View>
                </TouchableWithoutFeedback>
                <View style = {{marginTop: 45}}>
                    <Text style = {{fontSize: 20, fontWeight: 'bold', alignSelf: 'center', marginBottom: 10, color: '#2d3258', zIndex: 2}}>Today Tasks</Text>
                    <Carousel 
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.todayTasks}
                        renderItem={({item, index}) => 
                            <View style = {styles.tasksItem}>
                                <View style = {{backgroundColor: '#005581', width: '100%', padding: 5, flexDirection: 'row', justifyContent: 'space-between', borderTopRightRadius: 10, borderTopLeftRadius: 10}}>
                                    <Text style = {{fontSize: 18, fontWeight: 'bold', color: '#e6f9ff'}}>{(item.Subject.length > 20) ? item.Subject.slice(0,19) + "..." : item.Subject}</Text>
                                    <View style = {{alignItems: 'center',justifyContent: 'center', alignSelf: 'flex-end', width: 30, height: 20}}>
                                        <CheckBox
                                            center
                                            iconType='material'
                                            checkedIcon='check-box'
                                            uncheckedIcon='check-box-outline-blank'
                                            checkedColor='#27b46e'
                                            onPress={() => {{
                                                const newIds = this.state.checked.slice() 
                                                const task = this.state.todayTasks;
                                                newIds[index] = !this.state.checked[index]
                                                this.setState({checked: newIds})
                                                var newDone = 0;
                                                console.log(this.state.todayTasksNumber)
                                                for (let i = 0; i < this.state.todayTasksNumber; i++) {
                                                    if (newIds[i]) {
                                                        task[i].Status = "Complete"
                                                        newDone += 1;
                                                    } else {
                                                        task[i].Status = "Incomplete"
                                                    }
                                                }
                                                this.setState({
                                                    doneTasksNumber: newDone,
                                                    todayTasks: task
                                                })
                                                {this.updateTaskTable(task[index])}
                                            }}}
                                            checked={this.state.checked[index]}
                                        />
                                    </View>
                                </View>
                                <View style = {{flexDirection: 'row', justifyContent: 'space-between', width: '100%', padding: 5, alignItems: 'center'}}>
                                    <View style = {{flexDirection: 'row', alignItems: 'center'}}>
                                        <Icon name = "spinner" size = {15} color = {'black'}/>
                                        <Text style = {{marginLeft: 5}}>{item.Status}</Text>
                                    </View>
                                    <View style = {{flexDirection: 'row', alignItems: 'center'}}>
                                        <View style = {(item.Priority === "high") ? styles.highTask : (item.Priority === "normal") ? styles.normalTask: styles.lowTask}/>
                                        <Text>{item.Priority}</Text>
                                    </View>
                                </View>
                                <View>
                                    <Text style = {{fontSize: 16}}>{item.Lead}</Text>
                                </View>
                            </View>
                        }
                        sliderWidth={screenWidth}
                        itemWidth={screenWidth / 1.5}
                    />
                    <View style = {{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 10, paddingHorizontal: screenWidth / 7}}>
                        <View style ={{ width: 120, height: 120, borderRadius: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: '#29490d', marginVertical: 5}}>
                            <Text style = {{fontSize: 35, fontWeight: 'bold', color: '#e6f9ff'}}>{this.state.day}</Text>
                            <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#e6f9ff'}}>{this.state.monthInText}</Text>
                        </View>
                        <View style = {{alignItems: 'center', marginVertical: 5}}>
                            <AnimatedCircularProgress
                                size={120}
                                width={15}
                                fill={(this.state.doneTasksNumber == 0) ? 0 : Math.round(this.state.doneTasksNumber / this.state.todayTasksNumber * 100)}
                                tintColor="#27b46e"
                                backgroundColor="#3d5875" 
                            >
                            { (fill) => ( <Text style = {{fontSize: 25, fontWeight: 'bold'}}> { Math.round(fill) } {"%"} </Text> ) }
                            </AnimatedCircularProgress>
                        </View>
                    </View>
                </View>      
            </View>
        );
    }
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    lottie: {
        width: 100,
        height: 100
    },
    background: {
        backgroundColor: "rgba(0,0,0,.7)",
        position: 'absolute',
        bottom: 10,
        left: 10,
        alignSelf:'center',
        borderRadius: 30,
        width: 60,
        height: 60,
        zIndex: 3,
    },
    profileContainer: {
        height: '45%',
        width: screenWidth,
    },
    detailContainer: {
        padding: 10,
        backgroundColor: 'rgba(221,221,221,0.4)',
        borderRadius: 30,
        width: '80%',
        position: 'absolute',
        top: 35,
        alignSelf: 'center',
        alignItems: 'center',
        zIndex: 4
    },
    profiledetials: {
        fontSize: 18,
        marginBottom: '1%',
    },
    profiledetialsName: {
        fontSize: 22,
        marginBottom: '1%',
        fontWeight: 'bold'
    },
    avatar: {
        width: '100%',
        height: '100%',
        zIndex: 1,
    },
    button: {
        width: 60,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        borderRadius: 30,
        backgroundColor: '#27b46e',
        bottom: 10,
        left: 10, 
        zIndex: 4,
    },
    other: {
        alignSelf: 'center',
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        left: 20,
        borderRadius: 20,
        zIndex: 3
    },
    leadPlus: {
        backgroundColor: '#47c7eb'
    },
    productPlus: {
        backgroundColor: '#f03613'
    },
    taskPlus: {
        backgroundColor: '#fba01d'
    },
    bottomContainer: {
        flex: 1,
        backgroundColor: '#aaa',
        paddingHorizontal: 10
    },
    taskBox: {
        backgroundColor: 'gray',
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 40,
    },
    tasktitle: {
        alignItems: 'center'
    },
    circleBox: {
        marginTop: 40,
        alignItems: 'center',
        position: 'absolute',
        bottom: 10, 
        alignSelf: 'center'
    },
    label: {
        position: 'absolute',
        color: '#fff',
        fontSize: 16,
        width: 250,
        justifyContent: 'center',
        textAlignVertical:'center',
        zIndex: 2
    },
    avatarrounded: {
        borderWidth: 3,
        borderColor: '#1d7d9e'
    },
    number: {
        alignItems: 'center',
        width: '50%'
    },
    highTask: {
        width: 6, 
        height: 6, 
        borderRadius: 3,  
        marginRight: 5, 
        alignItems: 'center', 
        justifyContent: 'center', 
        backgroundColor: 'red',
    },
    normalTask: {
        width: 6, 
        height: 6, 
        borderRadius: 3,  
        marginRight: 5, 
        alignItems: 'center', 
        justifyContent: 'center', 
        backgroundColor: 'orange'
    },
    lowTask: {
        width: 6, 
        height: 6, 
        borderRadius: 3,  
        marginRight: 5, 
        alignItems: 'center', 
        justifyContent: 'center', 
        backgroundColor: 'green'
    },
    tasksItem: {
        borderWidth: 1,
        alignItems: 'center',
        backgroundColor: '#f3e0c2',
        borderRadius: 10
    }
})