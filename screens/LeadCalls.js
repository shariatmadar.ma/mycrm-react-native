import React, { Component } from 'react';
import {View, Text, StyleSheet, Dimensions, PermissionsAndroid, FlatList, Image } from 'react-native';
import CallLogs from 'react-native-call-log';
import Icon from 'react-native-vector-icons/FontAwesome';


const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class LeadCalls extends Component {
    
    static navigationOptions = ({navigation}) => ({
        title : 'Lead Calls',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
    })

    constructor (props) {
        super(props)
        this.state = {
            mobile: this.props.navigation.state.params.leadMobile,
            phone: this.props.navigation.state.params.leadPhone,
            allLogs: [],
            phoneCalls: [],
            mobileCalls: [],
        }
    }

    getLogs = async() => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
                {
                    title: 'Call Logs',
                    message:
                      'Access your call logs',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log(CallLogs);
                CallLogs.load(300).then(c => {
                    console.log(c)
                    const filteredData1 = c.filter(item => item.phoneNumber.includes(this.state.phone.slice(1)));
                    const filteredData2 = c.filter(item => item.phoneNumber.includes(this.state.mobile.slice(1)));
                    console.log("phone: ")
                    console.log(filteredData1)
                    console.log("mobile: ")
                    console.log(filteredData2)
                    this.setState({ 
                        phoneCalls: filteredData1,
                        mobileCalls: filteredData2
                    })
                });
            } else {
                console.log('Call Log permission denied');
            }
        }
        catch (e) {
            console.log(e);
        }
    }

    componentDidMount () {
        this.getLogs()
    }

    _renderItem = ({item, index}) => {
        return(
            <View style = {{
                flexDirection: 'row', 
                
            }}>
                <View style = {{justifyContent: 'center', alignItems: 'center', margin: 7}}>
                    <Image 
                        source = {(item.type == "INCOMING") ? require('../pics/incoming.png') 
                        : (item.type == "OUTGOING") ? require('../pics/outgoing.png')
                        : (item.type == "MISSED") ? require('../pics/missed.png')
                        : require ('../pics/unknown.png') } 
                    />
                </View>
                <View style = {{
                    flex: 1,
                    marginVertical: 2,
                    paddingVertical: 2,
                    borderBottomColor: "rgba(0,0,0,0.2)",
                    borderBottomWidth: 1
                }}>
                    <Text style = {{fontWeight: 'bold'}}>{item.name}</Text>
                    <Text>{item.dateTime}</Text>
                </View>
            </View>
        )
    }

    render () {
        return(
            <View style = {styles.container}>
                <View style = {{
                    borderWidth: 1,
                    margin: 5,
                    padding: 5,
                    // alignItems: 'center', 
                    backgroundColor: "rgba(243, 226, 179, 0.7)",
                    justifyContent: 'space-between',
                    borderRadius: 5,
                    elevation: 3,
                    flexDirection: 'row',
                    alignItems: 'center'
                }}>
                    <Text style = {{fontSize: 18, fontWeight:'bold', color: '#2aa1b7'}}>Mobile Calls</Text>
                    <Text style = {{fontSize: 18, fontWeight:'bold', color: '#2aa1b7'}}>{this.state.mobile}</Text>
                </View>
                <View style = {{
                    flex: 1,
                    borderWidth: 1,
                    margin: 5, 
                    padding: 5,
                    backgroundColor: '#fff',
                    borderRadius: 5,
                }}>
                    <FlatList 
                        data = {this.state.mobileCalls}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {this._renderItem}
                    />
                </View>
                <View style = {{
                    borderWidth: 1,
                    margin: 5,
                    padding: 5,
                    alignItems: 'center', 
                    borderRadius: 5,
                    elevation: 3,
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    backgroundColor: "rgba(243, 226, 179, 0.7)"
                }}>
                    <Text style = {{fontSize: 18, fontWeight:'bold', color: '#2aa1b7'}}>Phone Calls</Text>
                    <Text style = {{fontSize: 18, fontWeight:'bold', color: '#2aa1b7'}}>{this.state.phone}</Text>
                </View>
                <View style = {{
                    flex: 1,
                    borderRadius: 5,
                    borderWidth: 1,
                    padding: 5,
                    margin: 5,
                    backgroundColor: '#fff'
                }}>
                    <FlatList 
                        data = {this.state.phoneCalls}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {this._renderItem}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    }
})