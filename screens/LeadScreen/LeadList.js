import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Alert, Modal, Dimensions, TextInput, ToastAndroid } from 'react-native';
import { SearchBar, Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import SQLite from 'react-native-sqlite-storage';
import call from 'react-native-phone-call';
import {Fab} from 'native-base'
import SmsAndroid from 'react-native-get-sms-android';
import { Bars } from 'react-native-loader';
import MapView from 'react-native-maps'


const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

var db = SQLite.openDatabase({
    name: "CRMDatabase.db",
    createFromLocation: "~database.db",
    location: "Library"
})

export default class LeadList extends Component {

    constructor (props) {
        super(props);
        this.state = {
            search: '',
            allLeads: [],
            leads: [],
            isLoading: true,
            modalVisible: false,
            itemClicked : null,
            isVisible: false,
            modalSMSVisible: false,
            smsBody: '',
        }
    }

    returnData = (newLead) => {
        let newlead = this.state.allLeads.concat(newLead);
        this.setState({
            allLeads: newlead,
            leads: newlead
        });
    }

    returnUpdatedData = (updatedLead) => {
        let stableLeads = this.state.allLeads.filter(item => item.ID != updatedLead.ID);
        let updatedLeads = stableLeads.concat(updatedLead);
        this.setState({
            allLeads: updatedLeads,
            leads: updatedLeads,
        });
    }

    componentDidMount () {
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM Leads', [], (tx, result) => {
                console.log("Query completed");
                var data = [];
                var len = result.rows.length;
                console.log("Len:" + len);
                for (let i = 0; i < len; i++) {
                    let row = result.rows.item(i);
                    data.push(row);
                }
                this.setState({
                    allLeads: data,
                    leads: data,
                    isLoading: false,
                })
            })
        });
    }

    filterItems = (search) => {
        const filteredData = this.state.allLeads.filter(item => item.Name.includes(search));
        this.setState({ leads: filteredData })
        if (search == "") {
            this.setState({ leads: this.state.allLeads })
        }
    }

    deleteItem = (id) => {
        const filteredData = this.state.leads.filter(item => item.ID != id);
        this.setState({ leads: filteredData });
    }

    renderLeadName = (name, family) => {
        const fullName = name + " " + family;
        if (fullName.length < 30) {
            return (
                <Text style = {{fontSize: 16, fontWeight: 'bold', color: '#058fb2'}}>{fullName}</Text>
            )
        }
        return(
            <Text style = {{fontSize: 16, fontWeight: 'bold', color: '#058fb2'}}> {fullName.slice(0,25) + "..."} </Text>
        )
    }

    renderItem = ({item, index}) => {
        if (this.state.isVisible == true && index == this.state.itemClicked) {
            return (
            <View>
                <TouchableOpacity 
                    style = {[styles.leadsItem, {padding: 10}]} 
                    onPress = {() => this.setState({itemClicked: index, isVisible: !this.state.isVisible})}
                    onLongPress = {() => {
                        Alert.alert(
                            'Delete Lead',
                            'Are you sure to delete this lead?',
                            [
                                {text: 'NO', style: 'cancel'},
                                {text: 'YES', onPress: () => 
                                    {
                                        db.transaction(tx => {
                                            tx.executeSql('DELETE FROM Leads WHERE ID=?', [item.ID], (tx, result) => {
                                                console.log(item.ID)
                                                console.log("Lead Deleted");
                                            })
                                        });
                                        {this.deleteItem(item.ID)}
                                    }
                                },
                            ]
                        );
                        }}>
                        <Avatar
                            rounded
                            source = {(this.state.Image) ? {uri: this.state.Image} : null}
                            containerStyle = {styles.avatar}
                            title = {item.Name.slice(0,1) + item.Family.slice(0,1)}
                            size = {60}
                        />
                        <View>
                            <Text style = {{fontSize: 16, fontWeight: 'bold', color: '#058fb2'}}>{item.Name + " " + item.Family}</Text>
                            <Text style = {{color: '#1f5c70'}}>{item.Email}</Text>
                            <Text style = {{color: '#1f5c70'}}>{item.Mobile}</Text>
                        </View>
                </TouchableOpacity>
                <View style = {[styles.leadsItem, {justifyContent: 'space-between', paddingHorizontal: 20, paddingVertical: 10}]}>
                    <TouchableOpacity onPress = {() => {
                        this.props.navigation.navigate("LeadDetails", {lead: item})
                    }}>
                        <View>
                            <Icon name = "address-card" size = {25} color = {"#2aa1b7"}/>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress = {() => {
                            const args = {
                                number: item.Mobile,
                                prompt: false
                            };
                            call(args).catch(console.error)
                    }}>
                        <View>
                            <Icon name = "phone-square" size = {25} color = {"#2aa1b7"}/>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress = {() => this.setState({modalSMSVisible: !this.state.modalSMSVisible})}
                    >
                        <View>
                            <Icon name = "comments" size = {25} color = {"#2aa1b7"}/>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {() => this.setState({modalVisible: true})}>
                        <View>
                            <Icon name = "map-marker" size = {25} color = {"#2aa1b7"}/>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {() => {
                        this.props.navigation.navigate("editLead", {lead: item, returnUpdatedData: this.returnUpdatedData.bind(this)});
                        this.setState({isVisible: false})
                    }}>
                        <View>
                            <Icon name = "pencil-square" size = {25} color = {"#2aa1b7"}/>
                        </View>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType = "slide"
                    transparent = {true}
                    visible={this.state.modalSMSVisible}>
                        <View style = {{
                            backgroundColor: '#fff',
                            marginTop: 100,
                            paddingVertical: 5,
                            marginHorizontal: 15,
                            borderWidth: 3
                        }}>
                            <View 
                                style = {{
                                    flexDirection: 'row',
                                    marginHorizontal: 10 
                                }}>
                                <Text style = {{
                                    fontSize: 18,
                                    fontWeight: 'bold',
                                    color: '#2d3258'
                                }}>
                                    Send to: 
                                </Text>
                                <View style = {{
                                    marginLeft: 10
                                }}>
                                    <Text style = {{
                                        fontSize: 18,
                                        fontWeight: 'bold',
                                        color: '#2d3258'
                                    }}>
                                        {this.state.leads[this.state.itemClicked].Name} {this.state.leads[this.state.itemClicked].Family}
                                    </Text>
                                    <Text style = {{
                                        fontSize: 18,
                                        fontWeight: 'bold',
                                        color: '#2d3258'
                                    }}>
                                        {this.state.leads[this.state.itemClicked].Mobile}
                                    </Text>
                                </View>
                            </View>
                            <TextInput
                                style = {{
                                    borderWidth: 1,
                                    marginVertical: 5,
                                    marginHorizontal: 10,
                                    borderRadius: 10
                                }}
                                ref = {(input) => { this._BODY = input; }}
                                onChangeText={(text) => this.setState({smsBody: text})}
                                value={this.state.smsBody}
                                numberOfLines = {8}
                                placeholder = {"Enter message"} />  
                            <View style = {{
                                alignSelf: 'center',
                                flexDirection: 'row'
                            }}>
                                <TouchableOpacity
                                    style = {{
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        paddingVertical: 10,
                                        alignSelf: 'center',
                                        width: 100,
                                        borderRadius: 10, 
                                        backgroundColor: '#27b46e',
                                        marginHorizontal: 10
                                    }} 
                                    onPress = {() => {
                                        // console.log(this.state.leads[this.state.itemClicked].Mobile)
                                        const BODY = this.state.smsBody;
                                        const PHONE = this.state.leads[this.state.itemClicked].Mobile;
                                        SmsAndroid.autoSend(
                                            PHONE,
                                            BODY,
                                            (fail) => { 
                                                ToastAndroid.showWithGravityAndOffset(
                                                    'Message not sent',
                                                    ToastAndroid.SHORT,
                                                    ToastAndroid.BOTTOM,
                                                    0,
                                                    120
                                                );
                                             },
                                            (success) => { 
                                                ToastAndroid.showWithGravityAndOffset(
                                                    'Message sent succesfully',
                                                    ToastAndroid.SHORT,
                                                    ToastAndroid.BOTTOM,
                                                    0,
                                                    120
                                                );
                                            },
                                          );
                                        this._BODY.clear();
                                        this.setState({modalSMSVisible: !this.state.modalSMSVisible})
                                }}>
                                    <Text style = {{
                                        fontWeight: 'bold', 
                                        fontSize: 18, 
                                        color: '#fff'
                                    }}>
                                        Send
                                    </Text>    
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style = {{
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginHorizontal: 10,
                                        paddingVertical: 10,
                                        alignSelf: 'center',
                                        width: 100,
                                        borderRadius: 10, 
                                        // backgroundColor: '#27b46e',
                                        backgroundColor: '#f03613'
                                    }} 
                                    onPress = {() => {
                                        this.setState({modalSMSVisible: !this.state.modalSMSVisible})
                                }}>
                                    <Text style = {{
                                        fontWeight: 'bold', 
                                        fontSize: 18, 
                                        color: '#fff'
                                    }}>
                                        Cancel
                                    </Text>    
                                </TouchableOpacity>
                            </View>    
                        </View>
                </Modal>
                <Modal 
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}>
                    <View style = {{backgroundColor: '#fff', margin: 40, marginTop: screenHeight / 4.5 , padding: 5}}>
                        <View style = {{
                            position: 'absolute', 
                            bottom: 5, 
                            zIndex: 2, 
                            flexDirection: 'row',
                            alignSelf: 'center', 
                            alignItems: 'center', 
                            justifyContent: 'space-between',
                            flex: 1
                        }}>
                            <TouchableOpacity
                                style = {{
                                    marginHorizontal: 10
                                }}
                                onPress={() => {
                                    this.setState({modalVisible: !this.state.modalVisible});
                                }}>
                                        <Icon name = "times-circle" size = {40} color = {'#000'}/>
                            </TouchableOpacity>
                        </View>
                        <View style = {{
                            width: '100%',
                            height: 300,
                            // backgroundColor: '#000',
                            borderWidth: 1,
                            borderRadius: 5, 
                            marginBottom: 50
                        }}>
                            <MapView
                                ref = {(map) => { this._map = map; }}
                                style={styles.map}
                                region={{
                                    latitude: (this.state.leads[this.state.itemClicked].Lat) ? this.state.leads[this.state.itemClicked].Lat : 35.6892,
                                    longitude: (this.state.leads[this.state.itemClicked].Lng) ? this.state.leads[this.state.itemClicked].Lng : 51.3890,
                                    latitudeDelta: 0.1,
                                    longitudeDelta: 0.1,
                            }}>
                                <MapView.Marker coordinate={{"latitude": this.state.leads[this.state.itemClicked].Lat, "longitude": this.state.leads[this.state.itemClicked].Lng}} />
                            </MapView>
                        </View>
                    </View>
                </Modal>
            </View>
            )
        }
        return (
            <TouchableOpacity 
                style = {[styles.leadsItem, {padding: 10}]} 
                onPress = {() => this.setState({itemClicked: index, isVisible: !this.state.isVisible})}
                onLongPress = {() => {
                    Alert.alert(
                        'Delete Lead',
                        'Are you sure to delete this lead?',
                        [
                            {text: 'NO', style: 'cancel'},
                            {text: 'YES', onPress: () => 
                                {
                                    db.transaction(tx => {
                                        tx.executeSql('DELETE FROM Leads WHERE ID=?', [item.ID], (tx, result) => {
                                            console.log(item.ID)
                                            console.log("Lead Deleted");
                                        })
                                    });
                                    {this.deleteItem(item.ID)}
                                }
                            },
                        ]
                    );
                    }}>
                    <Avatar
                        rounded
                        source = {(this.state.Image) ? {uri: this.state.Image} : null}
                        containerStyle = {styles.avatar}
                        title = {item.Name.slice(0,1) + item.Family.slice(0,1)}
                        size = {60}
                    />
                    <View>
                        {this.renderLeadName(item.Name, item.Family)}
                        <Text style = {{color: '#1f5c70'}}>{item.Email}</Text>
                        <Text style = {{color: '#1f5c70'}}>{item.Mobile}</Text>
                    </View>
            </TouchableOpacity>
        )
    }

    render () {
        if (this.state.isLoading) {
            return (
                <View style = {[styles.container, {alignItems: 'center', justifyContent: 'center'}]}>
                    <Bars size={20} color="#000" />
                </View>
            )
        }
        if (this.state.allLeads.length === 0) {
            return (
                <View style = {styles.container}>
                    <SearchBar
                        containerStyle = {styles.searchBar}
                        inputContainerStyle = {styles.inputSearchBar}
                        inputStyle = {styles.inputSearch}
                        searchIcon = {<Icon name = 'search' size = {20} />}
                        placeholder = "Search ..."
                        onChangeText = {search => {this.setState({ search })}}
                        value = {this.state.search}
                    />
                    <View style = {styles.notFound}>
                        <View style = {styles.notFoundContainer}>
                            <Text style = {{fontSize: 18, fontWeight: 'bold', color: '#f03613'}}>Not Found!</Text>
                        </View>
                    </View>
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{ }}
                        style={{ backgroundColor: '#27b46e' }}
                        position="bottomRight"
                        onPress = {() => this.props.navigation.navigate("AddLead", {returnData: this.returnData.bind(this)})}>
                            <Icon name="plus" />
                    </Fab>  
                </View>
            )
        }
        return (
            <View style = {styles.container}>
                <SearchBar
                    containerStyle = {styles.searchBar}
                    inputContainerStyle = {styles.inputSearchBar}
                    inputStyle = {styles.inputSearch}
                    searchIcon = {<Icon name = 'search' size = {20} />}
                    placeholder="Search ..."
                    onChangeText = {search => {
                        this.setState({ search })
                        {this.filterItems(search)}
                    }}
                    // onChangeText={search => this.filterLeads(search)}
                    value={this.state.search}
                />
                <View style = {{marginTop: 5, marginHorizontal: 5, marginBottom: 120}}>
                    <FlatList
                        data = {this.state.leads}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {this.renderItem}
                    />
                </View>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#27b46e' }}
                    position="bottomRight"
                    onPress = {() => this.props.navigation.navigate("AddLead", {returnData: this.returnData.bind(this)})}>
                        <Icon name="plus" />
                </Fab> 
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    addLeadBtn: {
        borderBottomColor: '#05a3a4',
        marginHorizontal: 10,
        justifyContent: 'center',
        borderBottomWidth: 2,
        paddingVertical: 5,
    },
    addLeadTxt: {
        color: '#05a3a4',
        fontSize: 18,
        fontWeight: 'bold'
    },
    searchBar: {
        backgroundColor: 'transparent',
        borderBottomWidth: 0,
        borderTopWidth: 0,
    },
    inputSearchBar: {
        backgroundColor : '#fff',
        paddingHorizontal: 5,
        borderWidth:2,
        borderBottomWidth: 2,
        borderRadius: 50,
        borderColor: '#fcce01'
    },
    leadsItem: {
        alignItems: 'center',
        flexDirection: 'row',
        marginVertical: 8,
        borderRadius: 2,
        elevation: 5,
        backgroundColor: '#fff',
        marginHorizontal: 5,
        marginVertical: 5
    },
    avatar: {
        marginRight: 10,
        justifyContent: 'center',
        borderRadius: 10
    },
    notFound: {
        width: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1
    },
    notFoundContainer: {
        borderColor: '#f03613',
        width: 150, 
        height: 150, 
        borderWidth: 2, 
        // borderColor: '#526eda',
        alignSelf: 'center', 
        borderRadius: 75, 
        justifyContent: 'center',
        alignItems: 'center'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        // flex:1,
    },
})  