import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import MapView from 'react-native-maps'
import { Avatar } from 'react-native-elements';

let db = null ;

export default class AddLead extends Component {

    static navigationOptions = ({navigation}) => ({
        title : 'Edit Lead',
        headerStyle: {
            backgroundColor: '#1d7d9e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    constructor (props) {
        super(props);
        this.state = {
            nameFocus: false,
            familyFocus: false,
            phoneFocus: false,
            companyFocus: false,
            mobileFocus: false,
            siteFocus: false,
            addressFocus: false,
            descFocus: false,
            latFocus: false,
            lngFocus: false,
            ID: this.props.navigation.state.params.lead.ID,
            Name: this.props.navigation.state.params.lead.Name, 
            Family: this.props.navigation.state.params.lead.Family, 
            Phone: this.props.navigation.state.params.lead.Phone, 
            Company: this.props.navigation.state.params.lead.Company, 
            Email: this.props.navigation.state.params.lead.Email,
            Mobile: this.props.navigation.state.params.lead.Mobile, 
            AddressStreet: this.props.navigation.state.params.lead.Street, 
            AddressCity: this.props.navigation.state.params.lead.City, 
            AddressState: this.props.navigation.state.params.lead.State, 
            AddressZipCode: this.props.navigation.state.params.lead.ZipCode, 
            AddressCountry: this.props.navigation.state.params.lead.Country, 
            Desc: this.props.navigation.state.params.lead.Desc,
            Lat: this.props.navigation.state.params.lead.Lat, 
            Lng: this.props.navigation.state.params.lead.Lng,
            Image: this.props.navigation.state.params.lead.Image,
            marker: { 
                latlng: {
                    "latitude": 35.72125586791642, 
                    "longitude": 51.374745815992355
                }
            },
        }
    }

    openCB = () => {
        console.log("Database Opened.");
    }
    errorCB = (err) => {
        console.log("Error: " + err); 
    }

    update = () => {
        db = SQLite.openDatabase(
            {
                name: "CRMDatabase.db",
                createFromLocation: "~database.db",
                location: "Library"
            },
            this.openCB, 
            this.errorCB,
        );
        let lead = {
            ID: this.state.ID,
            Name: this.state.Name, 
            Family: this.state.Family, 
            Phone: this.state.Phone, 
            Company: this.state.Company, 
            Email: this.state.Email,
            Mobile: this.state.Mobile, 
            AddressStreet: this.state.AddressStreet, 
            AddressCity: this.state.AddressCity, 
            AddressState: this.state.AddressState,
            AddressZipCode: this.state.AddressZipCode, 
            AddressCountry: this.state.AddressCountry, 
            Desc: this.state.Desc,
            Lat: this.state.Lat, 
            Lng: this.state.Lng,
            Image: this.state.Image
        }
        db.transaction(tx => {
            tx.executeSql('UPDATE Leads SET Name = ?, Family = ?, Phone = ?, Company = ?, Email = ?, Mobile = ?, Street = ?, City = ?, State = ?, ZipCode = ?, Country = ?, Desc = ?, Lat = ?, Lng = ?, Image = ? WHERE ID = ?', 
                    [lead.Name, lead.Family, lead.Phone, lead.Company, lead.Email, lead.Mobile, 
                    lead.AddressStreet, lead.AddressCity, lead.AddressState, lead.AddressZipCode, 
                    lead.AddressCountry, lead.Desc, lead.Lat, lead.Lng, lead.ID, lead.Image]
            )
            console.log("Lead Added.")
        });
        this.props.navigation.state.params.returnUpdatedData(lead);
        this.props.navigation.goBack();
    }

    render () {
        return (
            <View style = {styles.container}>
                <View style = {{alignSelf: 'center', marginTop: 10}}>
                    <Avatar
                        rounded
                        showEditButton
                        onEditPress = {() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true
                              }).then(image => {
                                console.log(image);
                                this.setState({
                                    Image: image.path
                                })
                                console.log(this.state.avatarSource)
                            });
                        }}
                        source = {(this.state.Image) ? {uri: this.state.Image} : null}
                        containerStyle = {styles.avatar}
                        title = {this.state.Name.slice(0,1) + this.state.Family.slice(0,1)}
                        size = {100}
                    />
                </View>
                <ScrollView style = {{margin: 10}}>
                    <Text style = {styles.textNecessary}>Name</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Name: text})}
                        value={this.state.Name}
                        onFocus = {() => { this.setState({nameFocus: true}) }}
                        onBlur = {() => { this.setState({nameFocus: false}) }}
                        placeholder = {"Example: Mohammad"}
                        style = {(this.state.nameFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.textNecessary}>Family</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Family: text})}
                        value={this.state.Family}
                        onFocus = {() => { this.setState({familyFocus: true}) }}
                        onBlur = {() => { this.setState({familyFocus: false}) }}
                        placeholder = {"Example: Mohammadi"}
                        style = {(this.state.familyFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.textNecessary}>Phone</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Phone: text})}
                        value={this.state.Phone} 
                        onFocus = {() => { this.setState({phoneFocus: true}) }}
                        onBlur = {() => { this.setState({phoneFocus: false}) }}
                        placeholder = {"Example: 02155123456"}
                        style = {(this.state.phoneFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.textNecessary}>Mobile</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Mobile: text})}
                        value={this.state.Mobile} 
                        onFocus = {() => { this.setState({mobileFocus: true}) }}
                        onBlur = {() => { this.setState({mobileFocus: false}) }}
                        placeholder = {"Example: 09121234567"}
                        style = {(this.state.mobileFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.textNecessary}>Email</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Email: text})}
                        value={this.state.Email} 
                        onFocus = {() => { this.setState({siteFocus: true}) }}
                        onBlur = {() => { this.setState({siteFocus: false}) }}
                        placeholder = {"Example: MhdMohammadi@yahoo.com"}
                        style = {(this.state.siteFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.text}>Company Name</Text>
                    <TextInput 
                        onChangeText={(text) => this.setState({Company: text})}
                        value={this.state.Company}
                        onFocus = {() => { this.setState({companyFocus: true}) }}
                        onBlur = {() => { this.setState({companyFocus: false}) }}
                        placeholder = {"Example: Rasa Academy"}
                        style = {(this.state.companyFocus) ? styles.activeInput : styles.inactiveInput}/>
                    <Text style = {styles.textNecessary}>Location</Text>
                    <View style = {{
                        width: '100%',
                        height: 200,
                        borderWidth: 1,
                        borderRadius: 5
                    }}>
                        <MapView
                            ref = {(map) => { this._map = map; }}
                            style={styles.map}
                            onPress={(e) => {
                                this.setState({ 
                                    marker: { latlng: e.nativeEvent.coordinate },
                                    Lat: e.nativeEvent.coordinate.latitude,
                                    Lng: e.nativeEvent.coordinate.longitude
                                })
                            }}
                            region={{
                                latitude: 35.6892,
                                longitude: 51.3890,
                                latitudeDelta: 0.1,
                                longitudeDelta: 0.1,
                        }}>
                                <MapView.Marker coordinate={this.state.marker.latlng} />
                        </MapView>
                    </View>
                    <Text style = {styles.text}>Address</Text>
                    <View style = {(this.state.addressFocus) ? styles.activeAddressContainer : styles.inactiveAddressContainer}>
                        <TextInput
                            onChangeText={(text) => this.setState({AddressStreet: text})}
                            value={this.state.AddressStreet}
                            onFocus = {() => { this.setState({addressFocus: true}) }}
                            onBlur = {() => { this.setState({addressFocus: false}) }} 
                            placeholder = {"Street"}
                            style = {styles.addressInput}/>
                        <TextInput
                            onChangeText={(text) => this.setState({AddressCity: text})}
                            value={this.state.AddressCity}
                            onFocus = {() => { this.setState({addressFocus: true}) }}
                            onBlur = {() => { this.setState({addressFocus: false}) }} 
                            placeholder = {"City"}
                            style = {styles.addressInput}/>
                        <TextInput
                            onChangeText={(text) => this.setState({AddressState: text})}
                            value={this.state.AddressState}
                            onFocus = {() => { this.setState({addressFocus: true}) }}
                            onBlur = {() => { this.setState({addressFocus: false}) }} 
                            placeholder = {"State"}
                            style = {styles.addressInput}/>
                        <TextInput
                            onChangeText={(text) => this.setState({AddressZipCode: text})}
                            value={this.state.AddressZipCode}
                            onFocus = {() => { this.setState({addressFocus: true}) }}
                            onBlur = {() => { this.setState({addressFocus: false}) }} 
                            placeholder = {"Zip Code"}
                            style = {styles.addressInput}/>
                        <TextInput
                            onChangeText={(text) => this.setState({AddressCountry: text})}
                            value={this.state.AddressCountry}
                            onFocus = {() => { this.setState({addressFocus: true}) }}
                            onBlur = {() => { this.setState({addressFocus: false}) }} 
                            placeholder = {"Country"}
                            style = {styles.addressInput}/>                        
                    </View>
                    <Text style = {styles.text}>Description</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Desc: text})}
                        value={this.state.Desc}
                        numberOfLines = {5}
                        onFocus = {() => { this.setState({descFocus: true}) }}
                        onBlur = {() => { this.setState({descFocus: false}) }}
                        placeholder = {"Type here ..."}
                        style = {(this.state.descFocus) ? styles.activeInput : styles.inactiveInput}/>
                </ScrollView>
                <View style = {{alignItems: 'center', justifyContent: 'center', marginBottom: 10}}>
                    <TouchableOpacity style = {styles.submitBtn} onPress = {() => { this.update(); }}>
                        <Text style ={{fontWeight: 'bold', fontSize: 18, color: '#fff'}}>Submit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    text: {
        color: '#05a3a4',
        marginTop: 5,
        fontSize: 18,
        alignSelf: "center",
        fontWeight: 'bold'
    },
    textNecessary: {
        color: '#f03613',
        marginTop: 5,
        fontSize: 18,
        alignSelf: "center",
        fontWeight: 'bold'
    },
    inactiveInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    activeNecessaryInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#F08EA7'
    },
    activeInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#88bece'
    },
    inactiveAddressContainer: {
        padding: 10,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5
    },
    activeAddressContainer: {
        padding: 10,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#F08EA7'
    },
    addressInput: {
        paddingHorizontal: 10,
        borderStyle: 'dashed',
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    confirmBtn: {
        marginRight: 5,
    },
    submitBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000', 
        paddingVertical: 10,
        paddingHorizontal: 40,
        borderRadius: 10, 
        backgroundColor: '#27b46e',
    },
    mapBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        // alignSelf: 'center',
        borderColor: '#000', 
        borderWidth: 1,
        // width: '50%',
        padding: 10,
        borderRadius: 5, 
        backgroundColor: '#eee',
        marginVertical: 3
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        flex:1,
    },
})  