import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import { Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class LeadDetails extends Component {

    static navigationOptions = ({navigation}) => ({
        title : null,
        headerTransparent: true,
        headerStyle: {
        },
        headerTintColor: '#fff',
    })

    constructor (props) {
        super(props);
        this.state = {
            lead: this.props.navigation.state.params.lead,
        }
    }

    render () {
        return (
            <View style = {styles.container}>

                <View style = {{width: screenWidth * 2, height: screenWidth * 2, borderBottomRightRadius: screenWidth, position: 'absolute', top: -screenWidth* 1.3, left: -screenWidth * 1.1 ,backgroundColor: '#1d7d9e'}}>
                    <Image 
                        source = {require('../../pics/86373.png')}
                        style = {{position: 'absolute', zIndex: 2, opacity: 0.5}}
                    />
                </View>
                <Avatar
                    rounded
                    source = {(this.state.Image) ? {uri: this.state.Image} : null} 
                    containerStyle = {styles.avatar}
                    title = {this.state.lead.Name.slice(0,1) + this.state.lead.Family.slice(0,1)}
                    size = {screenWidth / 2.5}
                    avatarStyle = {{borderWidth: 3, borderColor: '#dab238', borderRadius: screenWidth / 5}}
                />

                <View style = {styles.profileName}>
                    <Text style = {{fontSize: 24, fontWeight: 'bold', color : '#2d3258'}}>{this.state.lead.Name + " " + this.state.lead.Family}</Text>
                </View>
                
                <View style = {styles.profileEmail}>
                    <Text style = {{fontSize: 14, color : '#058fb2'}}>{this.state.lead.Email}</Text>
                </View>

                <View style = {styles.profilePhone}>
                    <Text style = {{fontSize: 14, color : '#058fb2'}}>{this.state.lead.Mobile}</Text>
                </View>

                <View style = {{position: 'absolute', top: screenHeight * 0.5, right: screenHeight * 0.03, alignItems: 'center'}}>
                    <TouchableOpacity onPress = {() => this.props.navigation.navigate("LeadProfileDetails", {lead: this.state.lead})}>
                        <View style ={styles.navigationIcon}>
                            <Icon name = "id-badge" size = {40} color = {'#2aa1b7'}/>
                        </View>
                    </TouchableOpacity>
                    <Text style = {styles.navigationText}>Details</Text>
                </View>

                <View style = {{position: 'absolute', top: screenHeight * 0.7, right: screenWidth * 0.03, alignItems: 'center'}}>
                    <TouchableOpacity onPress = {() => {
                        console.log(this.state.lead)
                        this.props.navigation.navigate("LeadAttachments", {lead: this.state.lead})
                    }}>
                        <View style ={styles.navigationIcon}>
                            <Icon name = "folder-open" size = {40} color = {'#2aa1b7'}/>
                        </View>
                    </TouchableOpacity>
                    <Text style = {styles.navigationText}>Attachments</Text>
                </View>
                
                <View style = {{position: 'absolute', top: screenHeight * 0.5, alignSelf: 'center', alignItems: 'center'}}>
                    <TouchableOpacity onPress = {() => this.props.navigation.navigate("LeadCalls", {leadMobile: this.state.lead.Mobile, leadPhone: this.state.lead.Phone})}>
                        <View style ={styles.navigationIcon}>
                            <Icon name = "phone" size = {40} color = {'#2aa1b7'}/>    
                        </View>
                    </TouchableOpacity>
                    <Text style = {styles.navigationText}>Call</Text>
                </View>
                <View style = {{position: 'absolute', top: screenHeight * 0.7, alignSelf: 'center', alignItems: 'center'}}>
                    <TouchableOpacity onPress = {() => this.props.navigation.navigate("LeadSMS", {lead: this.state.lead})}>
                        <View style ={styles.navigationIcon}>
                            <Icon name = "comment" size = {40} color = {'#2aa1b7'}/>            
                        </View>
                    </TouchableOpacity>
                    <Text style = {styles.navigationText}>SMS</Text>
                </View>
                <View style = {{position: 'absolute', top: screenHeight * 0.5, left: screenWidth * 0.03, alignItems: 'center'}}>
                    <TouchableOpacity onPress = {() => this.props.navigation.navigate("LeadTasks", {leadName: this.state.lead.Name, leadFamily: this.state.lead.Family})}>
                        <View style ={styles.navigationIcon}>
                            <Icon name = "list-ul" size = {40} color = {'#2aa1b7'}/>    
                        </View>
                    </TouchableOpacity>
                    <Text style = {styles.navigationText}>Tasks</Text>
                </View>
                <View style = {{position: 'absolute', top: screenHeight * 0.7, left: screenWidth * 0.03, alignItems: 'center'}}>
                    <TouchableOpacity onPress = {() => this.props.navigation.navigate("LeadProduct", {lead: this.state.lead})}>
                        <View style ={styles.navigationIcon}>
                            <Icon name = "shopping-cart" size = {40} color = {'#2aa1b7'}/>    
                        </View>
                    </TouchableOpacity>
                    <Text style = {styles.navigationText}>Products</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    avatar: {
        position: 'absolute',
        top: screenHeight / 10,
        alignSelf: 'center'
    },
    profileName: {
        alignSelf: 'center',
        position: 'absolute',
        top: screenHeight / 2.9,
        alignItems: 'center',
        borderRadius: 40
    },
    profileEmail: {
        // backgroundColor: 'rgba(230, 249, 255, 0.5)',
        width: screenWidth,
        position: 'absolute',
        alignItems: 'center',
        top: screenHeight / 2.55,
    },
    profilePhone: {
        // backgroundColor: 'rgba(230, 249, 255, 0.5)',
        width: screenWidth,
        position: 'absolute',
        alignItems: 'center',
        top: screenHeight / 2.35,
    },
    navigationText: {
        marginTop: 2,
        fontSize: 16,
        color: '#2d3258',
        fontWeight: 'bold',
    },
    navigationIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth / 5, 
        height: screenWidth / 5, 
        borderRadius: screenWidth / 10, 
        backgroundColor: 'rgba(243, 226, 179, 0.7)', 
    }
})