import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, FlatList, TouchableOpacity } from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { SearchBar, Avatar, CheckBox } from 'react-native-elements';

var db = SQLite.openDatabase({
    name: "CRMDatabase.db",
    createFromLocation: "~database.db",
    location: "Library"
})

export default class LeadTasks extends Component {

    static navigationOptions = ({navigation}) => ({
        title : 'Lead Tasks',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
    })

    constructor (props) {
        super(props)
        this.state = {
            search: '',
            isLoading: true,
            leadTasks: [],
            allLeadTasks: [],
            lead: this.props.navigation.state.params.leadName + " " + this.props.navigation.state.params.leadFamily,
        }
    }

    filterItems = (search) => {
        const filteredData = this.state.allLeadTasks.filter(item => item.Subject.includes(search));
        this.setState({ leadTasks: filteredData })
        if (search == "") {
            this.setState({ leadTasks: this.state.allLeadTasks })
        }
    }

    componentDidMount () {
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM Tasks WHERE Lead LIKE ?', [this.state.lead], (tx, result) => {
                console.log("Query completed");
                var data = [];
                var len = result.rows.length;
                console.log("Len:" + len);
                for (let i = 0; i < len; i++) {
                    let row = result.rows.item(i);
                    data.push(row);
                }
                this.setState({
                    leadTasks: data,
                    allLeadTasks: data,
                    isLoading: false,
                })
            })
        });
    }

    renderStatus = (status) => {
        if (status == "completed") {
            return (<Icon name = "check-circle" size = {30} color = {'green'}/>)
        } else {
            return(<Icon name = "times-circle" size = {30} color = {'red'}/>)
        }
    }

    render () {
        if (this.state.isLoading == true) {
            <View style = {styles.container}>
                <ActivityIndicator />
            </View>
        }
        return (
            <View style = {styles.container}>
                <SearchBar
                    containerStyle = {styles.searchBar}
                    inputContainerStyle = {styles.inputSearchBar}
                    inputStyle = {styles.inputSearch}
                    searchIcon = {<Icon name = 'search' size = {20} />}
                    placeholder="Search ..."
                    onChangeText={search => {
                        this.setState({ search })
                        {this.filterItems(search)}
                    }}
                    value={this.state.search}
                />
                <View style = {{marginTop: 5, marginHorizontal: 5, marginBottom: 120}}>
                    <FlatList
                        numColumns = {2}
                        data = {this.state.leadTasks}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {({item, index}) =>
                            <TouchableOpacity style = {styles.tasksItem}>
                                <View style = {{flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <View>
                                        <Text style = {{fontSize: 16, fontWeight: 'bold'}}>{item.Subject}</Text>
                                        <Text style = {{color: '#777'}}>{item.Date}</Text>
                                        <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                            <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                                <View style = {(item.Priority === "high") ? styles.highTask : (item.Priority === "normal") ? styles.normalTask: styles.lowTask}/>
                                                <Text style ={{fontSize: 12, color: '#00281f', marginLeft: 5}}>{item.Priority}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style = {{alignSelf: 'center'}}>
                                        {this.renderStatus(item.Status)}
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    tasksItem: {
        flex: 1,
        marginHorizontal: 5,
        marginVertical: 5,
        borderColor: '#05a3a4',
        padding: 5,
        paddingHorizontal: 15,
        borderRadius: 2,
        justifyContent: 'center',
        elevation: 5,
        backgroundColor: '#fff',
    },
    highTask: {
        width: 6, 
        height: 6, 
        borderRadius: 3,  
        backgroundColor: 'red',
    },
    normalTask: {
        width: 6, 
        height: 6, 
        borderRadius: 3,  
        backgroundColor: 'orange'
    },
    lowTask: {
        width: 6, 
        height: 6, 
        borderRadius: 3,  
        backgroundColor: 'green'
    },
    searchBar: {
        backgroundColor: 'transparent',
        borderBottomWidth: 0,
        borderTopWidth: 0,
    },
    inputSearchBar: {
        backgroundColor : '#fff',
        paddingHorizontal: 5,
        borderWidth:2,
        borderBottomWidth: 2,
        borderRadius: 50,
        borderColor: '#fcce01'
    },
})  