import React , {Component} from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Text, PermissionsAndroid, TextInput, Dimensions, ToastAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import SmsAndroid from 'react-native-get-sms-android';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class LeadSMS extends Component {

    static navigationOptions = ({navigation}) => ({
        title : 'Lead SMS',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
    })

    constructor (props) {
        super(props)
        this.state = {
            SMSList: null,
            MySMSs: null,
            lead: this.props.navigation.state.params.lead,
            smsBody: ''
        }
    }

    SMSPermition = async() => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_SMS,
                {
                    title: 'SMS',
                    message: 'Access your SMS',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                }
            );
            if (granted == PermissionsAndroid.RESULTS.GRANTED) {
                var filter1 = {
                    box: 'inbox',
                    address: "+98" + this.state.lead.Mobile.slice(1),
                    indexFrom: 0,
                    maxCount: 30
                }
                var filter2 = {
                    box: 'inbox',
                    address: this.state.lead.Mobile,
                    indexFrom: 0,
                    maxCount: 30
                }
                var filter3 = {
                    box: 'sent',
                    address: "+98" + this.state.lead.Mobile.slice(1),
                    indexFrom: 0,
                    maxCount: 30
                }
                var filter4 = {
                    box: 'sent',
                    address: this.state.lead.Mobile,
                    indexFrom: 0,
                    maxCount: 30
                }
                SmsAndroid.list(
                    JSON.stringify(filter1),
                    (fail) => {
                        console.log('Failed with this error: ' + fail);
                    },
                    (count, smsList) => {
                        console.log(smsList)
                        this.setState({
                            SMSList: JSON.parse(smsList),
                        });
                    },
                );
                SmsAndroid.list(
                    JSON.stringify(filter2),
                    (fail) => {
                        console.log('Failed with this error: ' + fail);
                    },
                    (count, smsList) => {
                        console.log(".......................")
                        console.log(smsList)
                        const sms = this.state.SMSList.concat(JSON.parse(smsList))
                        this.setState({
                            SMSList: sms,
                        });
                    },
                );
                SmsAndroid.list(
                    JSON.stringify(filter3),
                    (fail) => {
                        console.log('Failed with this error: ' + fail);
                    },
                    (count, smsList) => {
                        console.log(smsList)
                        const sms = this.state.SMSList.concat(JSON.parse(smsList))
                        this.setState({
                            SMSList: sms,
                        });
                    },
                );
                SmsAndroid.list(
                    JSON.stringify(filter4),
                    (fail) => {
                        console.log('Failed with this error: ' + fail);
                    },
                    (count, smsList) => {
                        console.log(JSON.parse(smsList))
                        const sms = this.state.SMSList.concat(JSON.parse(smsList))
                        sms.sort(this.compare)
                        console.log("sms", sms)
                        this.setState({
                            SMSList: sms,
                        });
                    },
                );
            } else {
                console.log('SMS permission denied');
            }
        }
        catch (e) {
            console.log(e)
        }
    }

    compare = (a, b) => {
        const dateA = a.date;        
        const dateB = b.date;
        let comparison = 0;
        if (dateA > dateB) {
            comparison = 1;
        } else if (dateA < dateB) {
            comparison = -1;
        }
        return comparison;
    }
    
    componentDidMount () {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            this.SMSPermition();
        })
    }

    render () {
        return (
            <View style = {styles.container}>
                <View style = {{
                    flex: 1,
                    marginHorizontal: 5,
                    marginTop: 5,
                    marginBottom: 10
                }}>
                    <FlatList
                        data = {this.state.SMSList}
                        renderItem = {({item}) =>
                            <View style = {
                                (item.type == 1) ? {
                                    padding: 10,
                                    backgroundColor: '#fff',
                                    margin: 5,
                                    borderWidth: 0.2,
                                    borderRadius: 10
                                } 
                                : {
                                    padding: 10,
                                    backgroundColor: 'rgba(243, 226, 179, 0.7)',
                                    margin: 5,
                                    borderRadius: 10,
                                    borderWidth: 0.2
                                } 
                            }>
                                <Text>{item.body}</Text>
                            </View>
                        }
                    />
                </View>
                <View style = {{
                    flex: 1,
                    flexDirection: 'row',
                    width: screenWidth,
                    position: 'absolute',
                    bottom: 0,
                    backgroundColor: '#fff',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                }}>
                    <TextInput
                        ref = {(input) => { this._BODY = input; }}
                        style = {{flexL: 1}}
                        onChangeText={(text) => this.setState({smsBody: text})}
                        value={this.state.smsBody} 
                        placeholder = "Enter message"
                    />
                    <TouchableOpacity 
                        style = {{marginRight: 5}}
                        onPress = {() => {
                            const BODY = this.state.smsBody;
                            const PHONE = this.state.lead.Mobile;
                            SmsAndroid.autoSend(
                                PHONE,
                                BODY,
                                (fail) => { 
                                    ToastAndroid.showWithGravityAndOffset(
                                        'Message not sent',
                                        ToastAndroid.SHORT,
                                        ToastAndroid.BOTTOM,
                                        0,
                                        120
                                    );
                                 },
                                (success) => { 
                                    ToastAndroid.showWithGravityAndOffset(
                                        'Message sent succesfully',
                                        ToastAndroid.SHORT,
                                        ToastAndroid.BOTTOM,
                                        0,
                                        120
                                    );
                                },
                              );
                            this._BODY.clear();
                            this.setState({modalSMSVisible: !this.state.modalSMSVisible})
                    }}>
                        <View style = {{
                            padding: 5,
                            borderRadius: 5,
                            backgroundColor: '#27b46e',
                        }}>
                            <Text style = {{color: '#fff', fontWeight: 'bold'}}>Send</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor: '#e6f9ff',
    },
});