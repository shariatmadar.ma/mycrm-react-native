import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Fab} from 'native-base'
import DocumentPicker from 'react-native-document-picker';
import FileViewer from 'react-native-file-viewer';

var db = SQLite.openDatabase({
    name: "CRMDatabase.db",
    createFromLocation: "~database.db",
    location: "Library"
})

export default class LeadAttachments extends Component {

    static navigationOptions = ({navigation}) => ({
        title : 'Lead Attachments',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
    })

    constructor (props) {
        super(props)
        this.state = {
            lead: this.props.navigation.state.params.lead,
            docs: [],
            isLoading: true
        }
    }

    componentDidMount () {
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM LeadAttachments WHERE LeadID = ? ', [this.state.lead.ID], (tx, result) => {
                console.log("Query completed");
                var data = [];
                var len = result.rows.length;
                console.log("Len:" + len);
                for (let i = 0; i < len; i++) {
                    let row = result.rows.item(i);
                    data.push(row);
                }
                this.setState({
                    docs: data,
                    isLoading: false,
                })
            })
        });
    }

    insertToDB = (result) => {
        console.log(result)
        db.transaction(tx => {
            tx.executeSql('INSERT INTO LeadAttachments (LeadID, fileName, fileType, filePath) VALUES (?, ?, ?, ?)',
                [result.LeadID, result.fileName, result.fileType, result.filePath], (tx, result) => {
                    console.log(result)
                }
            );
            console.log("added")
        })
    }

    filePick = async () => {
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.allFiles],
            });
            var newDoc = this.state.docs;
            var result = {
                LeadID: this.state.lead.ID,
                fileName: res.name,
                fileType: res.type,
                filePath: res.uri
            }
            // console.log(result);
            newDoc = newDoc.concat(result);
            this.setState({docs: newDoc})
            {this.insertToDB(result)}
          } catch (err) {
            console.log("error", err)
          }
    }

    render () {
        return (
            <View style = {styles.container}>
                <View style = {{marginTop: 5, marginHorizontal: 5}}>
                    <FlatList 
                        data = {this.state.docs}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {({item, index}) => 
                            <TouchableOpacity
                                // onLongPress = {()} 
                                onPress = {() => {
                                    console.log(item.filePath)
                                    console.log(item.fileType)
                                    const path = item.filePath
                                    FileViewer.open(path, { showOpenWithDialog: true })
                                    .then(() => {
                                    	console.log("success")
                                    })
                                    .catch(error => {
                                    	console.log(error)
                                    });
                                }}
                            >
                                <View style = {{
                                    margin: 5,
                                    padding: 10,
                                    elevation: 5,
                                    backgroundColor: '#fff',
                                    flexDirection: 'row',
                                    alignItems: 'center' 
                                }}>
                                    <Icon name = {
                                        (item.fileType.includes('audio')) ? "music-note" :
                                        (item.fileType.includes('video')) ? "videocam" :
                                        (item.fileType.includes('image')) ? "image" :
                                        (item.fileType.includes('pdf')) ? "picture-as-pdf" :
                                        // (item.fileType.includes('rar') || item.fileType.includes('zip')) ? "file-archive" :
                                        // (item.fileType.includes('word')) ? "file-word" :
                                        "insert-drive-file"
                                    }
                                    size = {18}
                                    />
                                    <Text style = {{marginLeft: 10}}>{item.fileName}</Text>
                                </View>
                            </TouchableOpacity>
                        }
                    />
                </View>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#27b46e' }}
                    position="bottomRight"
                    onPress={this.filePick}>
                        <Icon name="attach-file" />
                </Fab>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    }
})  