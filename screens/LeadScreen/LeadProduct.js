import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Modal, ScrollView, Alert } from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import { Bars } from 'react-native-loader';
import { Avatar, CheckBox, SearchBar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Fab} from 'native-base'


var db = SQLite.openDatabase({
    name: "CRMDatabase.db",
    createFromLocation: "~database.db",
    location: "Library"
})

export default class LeadProduct extends Component {

    constructor (props) {
        super(props);
        this.state = {
            lead: this.props.navigation.state.params.lead,
            leadProducts: [],
            leadProductsLen: 0,
            leadProductsIDs: [],
            LPIDs: [],
            isLoading: true,
            products: [],
            allLeadProducts: [],
            modalVisible: false,
            checked: null,
            adeddProducts: []
        }
    }
    static navigationOptions = ({navigation}) => ({
        title : 'Lead Products',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
    })

    componentDidMount () {
        db.transaction(tx => {
            tx.executeSql('SELECT ProductID FROM LeadProducts WHERE LeadID LIKE ?', [this.state.lead.ID], (tx, result) => {
                console.log("Query 1 completed");
                var data = [];
                var len = result.rows.length;
                console.log("LPI.Len:" + len);
                for (let i = 0; i < len; i++) {
                    let row = result.rows.item(i)
                    data.push(row);
                }
                this.setState({
                    leadProductsIDs: data,
                    isLoading: false,
                    leadProductsLen: len,
                })
                var IDs = [];
                for (let i = 0; i < this.state.leadProductsLen; i++) {
                    IDs.push(this.state.leadProductsIDs[i].ProductID)
                }
                this.setState({LPIDs: IDs})
                console.log("LPIDS: " + this.state.LPIDs)
            })
        });
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM Products WHERE ID IN (SELECT ProductID FROM LeadProducts WHERE LeadID LIKE ?)', [this.state.lead.ID], (tx, result) => {
                console.log("Query 3 Completed...");
                var data = [];
                var len = result.rows.length;
                console.log("LP.Len:" + len);
                for (let i = 0; i < len; i++) {
                    let row = result.rows.item(i);
                    data.push(row);
                }
                this.setState({
                    leadProducts: data,
                    allLeadProducts: data,
                    isLoading: false,
                })
            })
        });
    }
    
    addProductToLead = () => {
        const LP = this.state.leadProducts;
        for (let i = 0; i <= this.state.checked.length; i++) {
            if (this.state.checked[i]) {
                db.transaction(tx => {
                    tx.executeSql('INSERT INTO LeadProducts (ProductID, LeadID) VALUES (?, ?)', [this.state.products[i].ID, this.state.lead.ID])
                    console.log("LEAD PROD Added.")
                });
                LP.push(this.state.products[i]);
            }
        }
        this.setState({
            leadProducts: LP,
            allLeadProducts: LP,
        })
        this.setState({modalVisible: !this.state.modalVisible})
    }

    // filterItems = (search) => {
    //     const filteredData = this.state.allLeadTasks.filter(item => item.Subject.includes(search));
    //     this.setState({ leadTasks: filteredData })
    //     if (search == "") {
    //         this.setState({ leadTasks: this.state.allLeadTasks })
    //     }
    // }

    _renderItem = ({item, index}) => {
        return(
            <TouchableOpacity 
                style = {[styles.productsItem, {backgroundColor: '#fff'}]}
                onLongPress = {() => {
                    var id = item.ID
                    Alert.alert(
                        'Delete Product',
                        'Are you sure to delete this product?',
                        [
                            {text: 'NO', style: 'cancel'},
                            {text: 'YES', onPress: () => 
                                {
                                    db.transaction(tx => {
                                        tx.executeSql('DELETE FROM LeadProducts WHERE ProductID = ?', [id], (tx, result) => {
                                            console.log(id)
                                            console.log("Prod Deleted");
                                            var LP = this.state.leadProducts.filter(item => item.ID != id);
                                            var LP2 = this.state.products.push(item)
                                            this.setState({
                                                leadProducts: LP,
                                                products: LP2})
                                        })
                                        if (this.state.leadProducts.length == 0) {
                                            var temp = [];
                                            var tempProduct = {
                                                ID: 0, 
                                                Code: 0,
                                                Name: "TEMP",
                                                Activity: 1,
                                                Price: 0,
                                                Desc: "TEMP PRODUCT"
                                            }
                                            temp.push(tempProduct)
                                            this.setState({leadProducts: temp})
                                        }
                                    });
                                }
                            },
                        ]
                    );
                }}>
                    <Avatar
                        rounded
                        source = {{uri: item.Image}}
                        title = {item.Name.slice(0,2).toUpperCase()}
                        containerStyle = {styles.avatar}
                        size = {60}/>
                    <View>
                        <Text style = {{fontSize: 16, fontWeight: 'bold', alignSelf: 'center'}}>
                            {(item.Name.length > 15) ? item.Name.slice(0,15) + "..." : item.Name}
                        </Text>
                        <Text style = {{color: '#777', alignSelf: 'center'}}>{item.Price + " $"}</Text>
                        {this.renderActivity(item.Activity)}
                    </View>
            </TouchableOpacity>
        )
    }

    
    renderActivity = (Activity) => {
        if (Activity == 1) {
            return (<Text style = {{color: 'green'}}>Active</Text>);
        } else {
            return (<Text style = {{color: 'red'}}>Not Active</Text>)
        }
    }

    render () {
        if (this.state.isLoading) {
            return (
                <View style = {[styles.container, {alignItems: 'center', justifyContent: 'center'}]}>
                    <Bars size={20} color="#000" />
                </View>
            );
        }
        return (
            <View style = {styles.container}>
                <SearchBar
                    containerStyle = {styles.searchBar}
                    inputContainerStyle = {styles.inputSearchBar}
                    inputStyle = {styles.inputSearch}
                    searchIcon = {<Icon name = 'search' size = {20} />}
                    placeholder="Search ..."
                    onChangeText={search => {
                        this.setState({ search })
                        {this.filterItems(search)}
                    }}
                    value={this.state.search}
                />
                <View style = {{marginTop: 5, marginHorizontal: 5, marginBottom: 120}}>
                    <FlatList 
                        numColumns = {2}
                        data = {this.state.leadProducts}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {this._renderItem}
                    />
                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}>
                        <View style = {{backgroundColor: '#fff', margin: 40, padding: 5}}>
                            <View 
                                style = {{
                                    position: 'absolute', 
                                    bottom: 5, 
                                    zIndex: 2, 
                                    flexDirection: 'row',
                                    alignSelf: 'center', 
                                    alignItems: 'center', 
                                    justifyContent: 'space-between',
                                    flex: 1
                                }}>
                                <TouchableOpacity
                                    style = {{
                                        marginHorizontal: 10
                                    }}
                                    onPress={() => {
                                        this.setState({modalVisible: !this.state.modalVisible});
                                    }}>
                                            <Icon name = "times-circle" size = {40} color = {'#000'}/>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style = {{
                                        marginHorizontal: 10
                                    }}
                                    onPress={this.addProductToLead}>
                                            <Icon name = "check-circle" size = {40} color = {'#000'}/>
                                </TouchableOpacity>
                            </View>
                            <View style = {{marginBottom: 50}}>
                                <FlatList 
                                    data = {this.state.products}
                                    keyExtractor = {(item, index) => index.toString()}
                                    renderItem = {({item, index}) =>
                                        <View style = {[styles.productsItem, {backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'space-between'}]}>
                                            <View style = {{flexDirection: 'row'}}>
                                                <Avatar
                                                    rounded
                                                    source = {{uri: item.Image}}
                                                    title = {item.Name.slice(0,2).toUpperCase()}
                                                    containerStyle = {styles.avatar}
                                                    size = {60}/>
                                                <View 
                                                    style = {{
                                                        marginLeft: 10
                                                }}>
                                                    <Text style = {{fontSize: 16, fontWeight: 'bold'}}>
                                                        {(item.Name.length > 15) ? item.Name.slice(0,15) + "..." : item.Name}
                                                    </Text>
                                                    <Text style = {{color: '#777'}}>{item.Price + " $"}</Text>
                                                    {this.renderActivity(item.Activity)}
                                                </View>
                                            </View>
                                            <View style = {{alignItems: 'center', justifyContent: 'center', width: 30, height: 30}}>
                                                <CheckBox
                                                    center
                                                    iconType='material'
                                                    size = {30}
                                                    checkedIcon='add-circle'
                                                    uncheckedIcon='cancel'
                                                    checkedColor='#27b46e'
                                                    onPress={() => {{
                                                        const newIds = this.state.checked.slice() 
                                                        newIds[index] = !this.state.checked[index]
                                                        this.setState({checked: newIds}) 
                                                    }}}
                                                    checked={this.state.checked[index]}
                                                />
                                            </View>
                                        </View>
                                    }
                                />
                            </View>
                        </View>
                </Modal>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#27b46e' }}
                    position="bottomRight"
                    onPress={() => {
                        this.setState({modalVisible: true})
                        db.transaction(tx => {
                            tx.executeSql('SELECT * FROM Products WHERE ID NOT IN (SELECT ProductID FROM LeadProducts WHERE LeadID LIKE ?)', [this.state.lead.ID], (tx, result) => {
                                console.log("Query 2 Completed");
                                var data = [];
                                var len = result.rows.length;
                                console.log("P.Len:" + len);
                                for (let i = 0; i < len; i++) {
                                    let row = result.rows.item(i);
                                    data.push(row);
                                }
                                this.setState({
                                    products: data,
                                    isLoading: false,
                                });
                            })
                        });
                        var selectedItems = new Array(this.state.products.length);
                        selectedItems.fill(false);
                        this.setState({
                            modalVisible: true,
                            checked: selectedItems,
                        })
                        }}>
                        <Icon name="plus" />
                </Fab>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    productsItem: {
        flex: 1,
        alignItems: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        padding: 5,
        borderRadius: 2,
        elevation: 5,
    },
    avatar: {
        justifyContent: 'center',
    },
    searchBar: {
        backgroundColor: 'transparent',
        borderBottomWidth: 0,
        borderTopWidth: 0,
    },
    inputSearchBar: {
        backgroundColor : '#fff',
        paddingHorizontal: 5,
        borderWidth:2,
        borderBottomWidth: 2,
        borderRadius: 50,
        borderColor: '#fcce01'
    },
})  