import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';

export default class LeadProfileDetails extends Component {
    
    static navigationOptions = ({navigation}) => ({
        title : 'Lead Details',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
    })
    
    constructor (props) {
        super(props);
        this.state = {
            lead: this.props.navigation.state.params.lead,
        }
    }

    render () {
        return (
            <ScrollView style = {styles.container}>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Name: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.Name}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Family: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.Family}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Phone: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.Phone}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Mobile: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.Mobile}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Email: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.Email}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Company: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.Company}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Location: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >({Math.round(this.state.lead.Lat)} , {Math.round(this.state.lead.Lng)})</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Street: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.Street}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >City: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.City}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >State: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.State}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >ZipCode: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.ZipCode}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Country: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.Country}</Text>
                </View>
                <View style = {styles.field}>
                    <Text style ={{fontSize: 18, fontWeight: 'bold', color: '#2d3258'}} >Desc: </Text>
                    <Text style ={{fontSize: 16, color: '#2aa1b7'}} >{this.state.lead.Desc}</Text>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    field: {
        flexDirection: 'row',
        padding: 5,
        margin: 5, 
        borderWidth: 1,
        alignItems: 'center',
        borderRadius: 5,
        elevation: 5,
        backgroundColor: '#fff'
    }
})  