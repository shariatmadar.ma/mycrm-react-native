import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity, Image, Picker } from 'react-native';
import DatePicker from 'react-native-datepicker';
import SQLite from 'react-native-sqlite-storage';
let db = null ;  

export default class AddProduct extends Component {
    
    static navigationOptions = ({navigation}) => ({
        title : 'Add Task',
        headerStyle: {
            backgroundColor: '#1d7d9e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })
    
    constructor (props) {
        super(props);
        this.state = {
            isSwitchOn : true,
            subjectFocus: false,
            statusFocus: false,
            priorityFocus: false,
            descFocus: false,
            leads: [],
            Subject: '',
            Date: '',
            Status: 'Incomplete',
            Priority: 'high',
            Desc: '',
            Lead: '',
        }
    }

    componentDidMount () {
        let db = SQLite.openDatabase(
            {
                name: "CRMDatabase.db",
                createFromLocation: "~database.db",
                location: "Library"
            },
            this.openCB, 
            this.errorCB,
        );
        db.transaction(tx => {
            tx.executeSql('SELECT ID, Name, Family FROM Leads', [], (tx, result) => {
                console.log("Query completed");
                var data = [];
                var len = result.rows.length;
                console.log("Len:" + len);
                for (let i = 0; i < len; i++) {
                    let row = result.rows.item(i);
                    data.push(row);
                }
                this.setState({
                    leads: data,
                    isLoading: false,
                })
            })
        });
    }

    add = () => {
        let task = {
            Subject: this.state.Subject, 
            Date: this.state.Date, 
            Status: this.state.Status, 
            Priority: this.state.Priority, 
            Desc: this.state.Desc,
            Lead: this.state.Lead,
        }
        if (task.Subject.length == 0) {
            alert("Subject is required");
            this._subject.focus();
        } else if (task.Date == '') {
            alert("Date is required")
        } else {
            db = SQLite.openDatabase(
                {
                    name: "CRMDatabase.db",
                    createFromLocation: "~database.db",
                    location: "Library"
                },
                this.openCB, 
                this.errorCB,
            );
            
            db.transaction(tx => {
                tx.executeSql(
                    'INSERT INTO Tasks (Subject, Date, Status, Priority, Desc, Lead) VALUES (?, ?, ?, ?, ?, ?)', 
                    [task.Subject, task.Date, task.Status, task.Priority, task.Desc, task.Lead]
                )
                console.log("Task Added.")
            });
            this.props.navigation.state.params.returnData(task);
            this.props.navigation.goBack();
        }
    }

    render () {
        return (
            <View style = {styles.container}>
                <ScrollView>
                    <Text style = {styles.textNecessary}>Subject</Text>
                    <TextInput
                        ref = {(input) => { this._subject = input; }}
                        onChangeText={(text) => this.setState({Subject: text})}
                        value={this.state.Subject}
                        onFocus = {() => { this.setState({subjectFocus: true}) }}
                        onBlur = {() => { this.setState({subjectFocus: false}) }} 
                        placeholder = {"Example: Meetting"}
                        style = {(this.state.subjectFocus) ? styles.activeNecessaryInput : styles.inactiveInput}
                    />
                    <Text style = {styles.textNecessary}>Due Date</Text>
                    <DatePicker
                        ref = {(input) => { this._date = input; }}
                        style = {{width: '100%', marginVertical: 3}}
                        date={this.state.Date}
                        mode="date"
                        placeholder="select date"
                        format="YYYY-MM-DD"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 5,
                            },
                            dateInput: {
                                fontSize: 16,
                                borderRadius: 5,
                                borderColor: '#000',
                                borderWidth: 1,
                            },
                        }}
                        onDateChange={(date) => {this.setState({Date: date})}}
                    />
                    <Text style = {styles.textNecessary}>Status</Text>
                    <Picker
                        ref = {(input) => { this._status = input; }}
                        selectedValue={this.state.Status}
                        style={{borderWidth: 1, width: '100%'}}
                        onValueChange={(itemValue, itemIndex) => this.setState({Status: itemValue})}>
                            <Picker.Item label="Incomplete" value="Incomplete" />
                            <Picker.Item label="Complete" value="Complete" />
                    </Picker>
                    <Text style = {styles.textNecessary}>Priority</Text>
                    <Picker
                        ref = {(input) => { this._priority = input; }}
                        selectedValue={this.state.Priority}
                        onValueChange={(itemValue, itemIndex) => this.setState({Priority: itemValue})}>
                            <Picker.Item label="High" value="high" />
                            <Picker.Item label="Normal" value="normal" />
                            <Picker.Item label="Low" value="low" />
                    </Picker>
                    <Text style = {styles.textNecessary}>Lead</Text>
                    <Picker
                        ref = {(input) => { this._lead = input; }}
                        selectedValue={this.state.Lead}
                        onValueChange={(itemValue, itemIndex) => this.setState({Lead: itemValue})}>
                            {this.state.leads.map(lead => <Picker.Item key={lead.ID} label={lead.Name + " " +  lead.Family} value={lead.Name + " " + lead.Family} />)}
                    </Picker>
                    <Text style = {styles.text}>Description</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Desc: text})}
                        value={this.state.Desc}
                        onFocus = {() => { this.setState({descFocus: true}) }}
                        onBlur = {() => { this.setState({descFocus: false}) }}
                        numberOfLines = {5} 
                        placeholder = {"Type here ..."}
                        style = {(this.state.descFocus) ? styles.activeInput : styles.inactiveInput}
                    />
                </ScrollView>
                <View style = {{alignItems: 'center', justifyContent: 'center'}}>
                    <TouchableOpacity style = {styles.submitBtn} onPress = {() => { this.add(); }}>
                        <Text style ={{fontWeight: 'bold', fontSize: 18, color: '#fff'}}>Submit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
        flex: 1,
    },
    confirmBtn: {
        marginRight: 5,
    },
    textNecessary: {
        color: '#f03613',
        marginTop: 5,
        fontSize: 18,
        alignSelf: "center",
        fontWeight: 'bold'
    },
    text: {
        color: '#05a3a4',
        marginTop: 5,
        fontSize: 18,
        alignSelf: "center",
        fontWeight: 'bold'
    },
    activeNecessaryInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#F08EA7', 
    },
    inactiveInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
    },
    activeInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#88bece',
    },
    activeSwitch: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 8,
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
        textAlignVertical: 'center'
    },
    textActive: {
        color: '#526eda',
        fontSize: 18,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    submitBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000', 
        paddingVertical: 10,
        paddingHorizontal: 40,
        borderRadius: 10, 
        backgroundColor: '#27b46e',
        marginTop: 15
    },
})  