import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, Alert } from 'react-native';
import { SearchBar, Avatar, CheckBox } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import SQLite from 'react-native-sqlite-storage';
import {Fab} from 'native-base'
import { Bars } from 'react-native-loader';

var db = SQLite.openDatabase({
    name: "CRMDatabase.db",
    createFromLocation: "~database.db",
    location: "Library"
})

export default class ProductList extends Component {
    constructor (props) {
        super(props);
        this.state = {
            search: '',
            tasks: [],
            isLoading: true,
            allTasks: [],
        }
    }

    returnData = (newTask) => {
        let newtask = this.state.tasks.concat(newTask);
        this.setState({
            tasks: newtask,
            allTasks: newtask});
    }

    renderStatus = (status) => {
        if (status == "Complete") {
            return (<Icon name = "check-circle" size = {30} color = {'green'}/>)
        } else {
            return(<Icon name = "times-circle" size = {30} color = {'red'}/>)
        }
    }

    filterItems = (search) => {
        const filteredData = this.state.allTasks.filter(item => item.Subject.includes(search));
        this.setState({ tasks: filteredData })
        if (search == "") {
            this.setState({ tasks: this.state.allTasks })
        }
    }


    componentDidMount () {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            db.transaction(tx => {
                tx.executeSql('SELECT * FROM Tasks', [], (tx, result) => {
                    console.log("Query completed");
                    var data = [];
                    var len = result.rows.length;
                    console.log("Len:" + len);
                    for (let i = 0; i < len; i++) {
                        let row = result.rows.item(i);
                        data.push(row);
                    }
                    this.setState({
                        tasks: data,
                        allTasks: data,
                        isLoading: false,
                    })
                })
            });
        })
    }

    deleteItem = (id) => {
        const filteredData = this.state.tasks.filter(item => item.ID != id);
        this.setState({ tasks: filteredData });
    }

    render () {
        if (this.state.isLoading) {
            return (
                <View style = {[styles.container, {alignItems: 'center', justifyContent: 'center'}]}>
                    <Bars size={20} color="#000" />
                </View>
            )
        }
        if (this.state.allTasks.length === 0) {
            return (
                <View style = {styles.container}>
                    <SearchBar
                        containerStyle = {styles.searchBar}
                        inputContainerStyle = {styles.inputSearchBar}
                        inputStyle = {styles.inputSearch}
                        searchIcon = {<Icon name = 'search' size = {20} />}
                        placeholder="Search ..."
                        onChangeText={search => { this.setState({ search }) }}
                        value={this.state.search}
                    />
                    <View style = {styles.notFound}>
                        <View style = {styles.notFoundContainer}>
                            <Text style = {{fontSize: 18, fontWeight: 'bold', color: '#f03613'}}>Not Found!</Text>
                        </View>
                    </View>
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{ }}
                        style={{ backgroundColor: '#27b46e' }}
                        position="bottomRight"
                        onPress = {() => this.props.navigation.navigate("AddTask", {returnData: this.returnData.bind(this)})}>
                            <Icon name="plus" />
                    </Fab>  
                </View>
            )
        }
        return (
            <View style = {styles.container}>
                <SearchBar
                    containerStyle = {styles.searchBar}
                    inputContainerStyle = {styles.inputSearchBar}
                    inputStyle = {styles.inputSearch}
                    searchIcon = {<Icon name = 'search' size = {20} />}
                    placeholder="Search ..."
                    onChangeText={search => {
                        this.setState({ search })
                        {this.filterItems(search)}
                    }}
                    value={this.state.search}
                />
                <View style = {{marginTop: 5, marginHorizontal: 5, marginBottom: 120}}>
                    <FlatList
                        numColumns = {2}
                        data = {this.state.tasks}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {({item}) => 
                            <TouchableOpacity 
                                style = {styles.tasksItem} 
                                onPress = {() => this.props.navigation.navigate("TaskDetails", {task: item})}
                                onLongPress = {() => {
                                    Alert.alert(
                                        'Delete Task',
                                        'Are you sure to delete this task?',
                                        [
                                            {text: 'NO', style: 'cancel'},
                                            {text: 'YES', onPress: () => 
                                                {
                                                    db.transaction(tx => {
                                                        tx.executeSql('DELETE FROM Tasks WHERE ID=?', [item.ID], (tx, result) => {
                                                            console.log(item.ID)
                                                            console.log("Task Deleted");
                                                        })
                                                    });
                                                    {this.deleteItem(item.ID)}
                                                }
                                            },
                                        ]
                                    );
                            }}>
                                <View style = {{flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <View>
                                        <Text style = {{fontSize: 16, fontWeight: 'bold'}}>{(item.Subject.length < 12 ) ? item.Subject : item.Subject.slice(0,11) + "..."}</Text>
                                        <Text style = {{color: '#777'}}>{item.Date}</Text>
                                        <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                            <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                                <View style = {(item.Priority === "high") ? styles.highTask : (item.Priority === "normal") ? styles.normalTask: styles.lowTask}/>
                                                <Text style ={{fontSize: 12, color: '#00281f', marginLeft: 5}}>{item.Priority}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style = {{alignSelf: 'center'}}>
                                        {this.renderStatus(item.Status)}
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }
                    />
                </View>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#27b46e' }}
                    position="bottomRight"
                    onPress = {() => this.props.navigation.navigate("AddTask", {returnData: this.returnData.bind(this)})}>
                        <Icon name="plus" />
                </Fab>  
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    addLeadBtn: {
        borderBottomColor: '#05a3a4',
        marginHorizontal: 10,
        paddingVertical: 5,
        justifyContent: 'center',
        borderBottomWidth: 2,
    },
    addTaskTxt: {
        fontSize: 18,
        color: '#05a3a4',
        fontWeight: 'bold'
    },
    searchBar: {
        backgroundColor: 'transparent',
        borderBottomWidth: 0,
        borderTopWidth: 0,
    },
    inputSearchBar: {
        backgroundColor : '#fff',
        paddingHorizontal: 5,
        borderWidth:2,
        borderBottomWidth: 2,
        borderRadius: 50,
        borderColor: '#fcce01'
    },
    tasksItem: {
        flex: 1,
        marginHorizontal: 5,
        marginVertical: 5,
        borderColor: '#05a3a4',
        padding: 5,
        paddingHorizontal: 15,
        borderRadius: 2,
        justifyContent: 'center',
        elevation: 5,
        backgroundColor: '#fff',
    },
    avatar: {
        justifyContent: 'center',
    },
    highTask: {
        width: 6, 
        height: 6, 
        borderRadius: 3,  
        backgroundColor: 'red',
    },
    normalTask: {
        width: 6, 
        height: 6, 
        borderRadius: 3,  
        backgroundColor: 'orange'
    },
    lowTask: {
        width: 6, 
        height: 6, 
        borderRadius: 3,  
        backgroundColor: 'green'
    },
    notFound: {
        width: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1
    },
    notFoundContainer: {
        width: 150, 
        height: 150, 
        borderWidth: 2, 
        borderColor: '#f03613',
        alignSelf: 'center', 
        borderRadius: 75, 
        justifyContent: 'center',
        alignItems: 'center'
    }
})  