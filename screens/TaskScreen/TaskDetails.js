import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LottieView from 'lottie-react-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class TaskDetails extends Component {

    static navigationOptions = ({navigation}) => ({
        title : 'Task Details',
        headerStyle: {
            backgroundColor: '#1d7d9e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    constructor (props) {
        super(props)
        this.state = {
            task: this.props.navigation.state.params.task,
            monthInNumber: parseInt(this.props.navigation.state.params.task.Date.slice(5,7)),
            monthInText: ''
        }
    }

    componentDidMount () {
        if (this.state.monthInNumber == 1) {
            this.setState({
                monthInText: "Jan"
            })
        } else if (this.state.monthInNumber == 2) {
            this.setState({
                monthInText: "Feb"
            })
        } else if (this.state.monthInNumber == 3) {
            this.setState({
                monthInText: "Mar"
            })
        } else if (this.state.monthInNumber == 4) {
            this.setState({
                monthInText: "Apr"
            })
        } else if (this.state.monthInNumber == 5) {
            this.setState({
                monthInText: "May"
            })
        } else if (this.state.monthInNumber == 6) {
            this.setState({
                monthInText: "Jun"
            })
        } else if (this.state.monthInNumber == 7) {
            this.setState({
                monthInText: "Jul"
            })
        } else if (this.state.monthInNumber == 8) {
            this.setState({
                monthInText: "Aug"
            })
        } else if (this.state.monthInNumber == 9) {
            this.setState({
                monthInText: "Sep"
            })
        } else if (this.state.monthInNumber == 10) {
            this.setState({
                monthInText: "Oct"
            })
        } else if (this.state.monthInNumber == 11) {
            this.setState({
                monthInText: "Nov"
            })
        } else if (this.state.monthInNumber == 12) {
            this.setState({
                monthInText: "Dec"
            })
        }
    }
    render () {
        return (
            <View style = {styles.container}>
                <View style = {{
                    borderBottomWidth: 2, 
                    borderBottomColor: '#2d3258', 
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <View style = {{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        width: screenWidth - 20,
                        alignItems: 'center',
                        margin: 10
                    }}>
                        <Text style = {{
                            fontSize: 22,
                            fontWeight: 'bold',
                            color: '#2d3258'
                        }}>{this.state.task.Subject}</Text>
                        <View style = {{flexDirection: 'row', alignSelf: 'center'}}>
                            <View style = {{marginRight: 5}}>
                                <Icon 
                                    name = {"check-circle"}
                                    size = {40}
                                    color = {(this.state.task.Status == "Complete") ? "green" : "gray"}/>
                            </View>
                            <View style = {{marginLeft: 5}}>
                                <Icon
                                    name = {"times-circle"}
                                    size = {40}
                                    color = {(this.state.task.Status == "Complete") ? "gray" : "red"}/>    
                            </View>
                        </View>
                    </View>
                </View>
                
                <ScrollView>
                    <View style = {{ 
                        width: 150, 
                        height: 150, 
                        borderRadius: 75,
                        alignSelf: 'center', 
                        alignItems: 'center', 
                        justifyContent: 'center', 
                        backgroundColor: '#29490d', 
                        marginTop: 20
                    }}>
                        
                        <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#e6f9ff'}}>{this.state.task.Date.slice(0,4)}</Text>
                        <Text style = {{fontSize: 40, fontWeight: 'bold', color: '#e6f9ff'}}>{this.state.task.Date.slice(8,10)}</Text>
                        <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#e6f9ff'}}>{this.state.monthInText}</Text>
                    </View>
                    <View style = {{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 35}}>
                        <View style = {
                            (this.state.task.Status == "Complete") ? styles.complete : styles.incomplete
                        }>
                            <Icon name = "battery-half" size = {20}/>
                            <Text style = {{fontWeight: 'bold', fontSize: 16}}>{this.state.task.Status}</Text>
                        </View>
                        {/* <LottieView 
                            source={require('../../lotties/9177-taskman.json')}
                            autoPlay 
                        /> */}
                        <View style = {
                            (this.state.task.Priority == "high") ? styles.high :
                            (this.state.task.Priority == "normal") ? styles.normal :
                            styles.low 
                        }>
                            <Icon name = "list-ol" size = {20}/>
                            <Text style = {{fontWeight: 'bold', fontSize: 16}}>{this.state.task.Priority}</Text>
                        </View>
                    </View>
                    <View style = {{marginHorizontal: 15, borderWidth: 2, marginTop: 15, padding: 10}}>
                        <View style = {{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            <Icon name = "user" size = {30}/>
                            <Text style = {{marginLeft: 10, fontSize: 18}}>{this.state.task.Lead}</Text>
                        </View>
                        <View style = {{
                            flexDirection: 'row',
                            marginTop: 20,
                            alignItems: 'center',
                        }}>
                            <Icon name = "sticky-note" size = {30}/>
                            <Text style = {{marginLeft: 10, fontSize: 18}}>{this.state.task.Desc}</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff'
    },
    normal: {
        width: 100,
        height: 100,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'orange'
    },
    high: {
        width: 100,
        height: 100,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red'
    },
    low: {
        width: 100,
        height: 100,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'green'
    },
    incomplete: {
        width: 100,
        height: 100,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red'
    },
    complete: {
        width: 100,
        height: 100,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'green'
    }
})  