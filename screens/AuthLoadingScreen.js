import React, { Component } from 'react';
import { View, StyleSheet } from "react-native";
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';


export default class AuthLoadingScreen extends Component {

    _userFind = async () => {
        const userToken = await AsyncStorage.getItem('userToken');
        console.log(userToken);
        this.props.navigation.navigate(userToken ? 'App' : 'Auth');
    }

    componentDidMount() {
        this._userFind();
    }

    render() {
        return (
            <View style = {styles.container}>
                <LottieView 
                    source={require('../lotties/196-material-wave-loading.json')} 
                    autoPlay 
                    loop
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center' 
    },
});
