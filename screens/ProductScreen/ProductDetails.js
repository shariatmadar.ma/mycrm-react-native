import React, { Component } from 'react';
import SQLite from 'react-native-sqlite-storage';
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions, Alert, ScrollView } from 'react-native';
import { Avatar } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import { Bubbles } from 'react-native-loader';

var db = SQLite.openDatabase({
    name: "CRMDatabase.db",
    createFromLocation: "~database.db",
    location: "Library"
})

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class ProductDetails extends Component {

    static navigationOptions = ({navigation}) => ({
        title : null,
        headerTransparent: true,
        headerStyle: {
        },
        headerTintColor: '#fff',
    })

    constructor (props) {
        super(props);
        this.state = {
            isLoading: true,
            product: this.props.navigation.state.params.product,
            leads: []
        }
    }

    componentDidMount () {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            db.transaction(tx => {
                tx.executeSql('SELECT * FROM Leads WHERE ID IN (SELECT LeadID FROM LeadProducts WHERE ProductID LIKE ?)', [this.state.product.ID], (tx, result) => {
                    console.log("Query 3 Completed...");
                    var data = [];
                    var len = result.rows.length;
                    console.log("LP.Len:" + len);
                    for (let i = 0; i < len; i++) {
                        let row = result.rows.item(i);
                        data.push(row);
                    }
                    this.setState({
                        leads: data,
                        isLoading: false,
                    })
                })
            });
        })
    }

    deleteProduct = () => {
        Alert.alert(
            'Delete Product',
            'Are you sure to delete this product?',
            [
                {text: 'NO', style: 'cancel'},
                {text: 'YES', onPress: () => 
                    {
                        let db = SQLite.openDatabase(
                            {
                                name: 'CRMDatabase.db', 
                                createFromLocation : "~database.db", 
                                location: 'Library'
                            }, 
                            this.openCB, 
                            this.errorCB
                        );
                        db.transaction(tx => {
                            tx.executeSql('DELETE FROM Products WHERE ID=?', [this.state.product.ID], (tx, result) => {
                                console.log("Product Deleted");
                            })
                        });
                        this.props.navigation.navigate("ProductList")
                    }
                },
            ]
          );
    }

    renderActivity = (Activity) => {
        if (Activity == 1) {
            return (<Text style = {{color: 'green', fontWeight: 'bold'}}>On</Text>);
        } else {
            return (<Text style = {{color: 'red', fontWeight: 'bold'}}>Off</Text>)
        }
    }

    render () {
        if (this.state.isLoading) {
            return(
                <View style = {[styles.container, {justifyContent: 'center', alignItems: 'center'}]}>
                    <Bubbles size={10} color="#000" />
                </View>
            )
        }
        return (
            <View style = {styles.container}>
                <View style = {styles.headerStyle}>
                    <Image 
                        source = {require('../../pics/86373.png')}
                        style = {{position: 'absolute', zIndex: 2, opacity: 0.5}}
                    />
                </View>
                <View style = {styles.header2Style}></View>
                <Avatar
                    source = {(this.state.product.Image) ? {uri: this.state.product.Image} : null}
                    containerStyle = {styles.avatarrounded}
                    title = {this.state.product.Name.slice(0,2).toUpperCase()}
                    size = {screenWidth / 2.5}
                    avatarStyle = {{borderWidth: 4, borderTopWidth: 4, borderColor: '#dab238'}}
                />
                <Text style = {styles.pName}>{this.state.product.Name}</Text>
                <ScrollView style = {{marginTop: screenHeight / 30}}>
                    <View style = {{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: screenWidth / 15}}>
                        <View style = {styles.details}>
                            <Text style = {{color: '#2aa1b7', fontWeight: 'bold', fontSize: 16}}>Code</Text>
                            <Text style = {{fontWeight: 'bold'}}>{this.state.product.Code}</Text>
                        </View>
                        <View style = {styles.details}>
                            <Text style = {{color: '#2aa1b7', fontWeight: 'bold', fontSize: 16}}>Price</Text>
                            <Text style = {{fontWeight: 'bold'}}>{this.state.product.Price + " $"}</Text>
                        </View>
                        <View style = {styles.details}>
                            <Text style = {{color: '#2aa1b7', fontWeight: 'bold', fontSize: 16}}>Activity</Text>
                            {this.renderActivity(this.state.product.Activity)}
                        </View>
                    </View>
                    <View style = {{marginTop: 15}}>
                        <Carousel 
                            ref={(c) => { this._carousel = c; }}
                            data={this.state.leads}
                            renderItem={({item, index}) => 
                                <View style = {styles.leadsItem}>
                                    <Avatar
                                        rounded
                                        source = {(item.Image) ? {uri: item.Image} : null}
                                        containerStyle = {styles.avatar}
                                        title = {item.Name.slice(0,1) + item.Family.slice(0,1)}
                                        size = {60}
                                    />
                                    <View style = {{justifyContent: 'center', alignItems: 'center'}}>
                                        <Text style = {{fontSize: 16, fontWeight: 'bold', color: '#2d3258', textAlign: 'center'}}>{item.Name + " " + item.Family}</Text>
                                        <Text style = {{color: '#1f5c70', textAlign: 'center'}}>{item.Email}</Text>
                                        <Text style = {{color: '#1f5c70', textAlign: 'center'}}>{item.Mobile}</Text>
                                    </View>
                                </View>
                            }
                            sliderWidth={screenWidth}
                            itemWidth={screenWidth / 2}
                        />
                    </View>
                    <View style = {styles.descContainer}>
                        <Text style = {styles.desc}>{this.state.product.Desc}</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    headerStyle: {
        width: screenWidth * 2,
        height: screenWidth * 2,
        backgroundColor: '#1d7d9e',
        borderRadius: screenWidth,
        top: -screenWidth * 1.5,
        alignSelf: 'center',
        position: 'absolute',
        zIndex: 2,
    },
    header2Style: {
        width: screenWidth * 2,
        height: screenWidth * 2,
        backgroundColor: 'rgb(241,241,241)',
        borderRadius: screenWidth,
        top: -screenWidth * 1.1,
        alignSelf: 'center',
        position: 'absolute'
    },
    avatarrounded: {
        zIndex: 3,
        alignSelf: 'center',
        marginTop: screenHeight/15,
    },
    pName: {
        alignSelf: 'center', 
        marginTop: 10,
        fontSize: 20,
        fontWeight: 'bold', 
        color: '#2d3258'
    },
    details: {
        backgroundColor: 'rgba(243, 226, 179, 0.7)', 
        width: 90, 
        height: 90,
        alignItems: 'center', 
        justifyContent: 'center', 
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#058fb2',
    },
    descContainer: {
        margin: 10,
        padding: 5,
        borderTopWidth: 2,
        borderTopColor: '#2d3258'
    },
    desc: {
        textAlign: 'justify'
    },
    leadsItem: {
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#f3e2b3',
        // borderStyle: 'dashed',
        padding: 5, 
        borderRadius: 10,
        backgroundColor: 'rgb(241,241,241)',
    }
})  