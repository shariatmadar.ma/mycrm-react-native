import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, Alert, Dimensions, Modal} from 'react-native';
import { SearchBar, Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Fab} from 'native-base'
import SQLite from 'react-native-sqlite-storage';
import { Bars } from 'react-native-loader';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

var db = SQLite.openDatabase({
    name: "CRMDatabase.db",
    createFromLocation: "~database.db",
    location: "Library"
})

export default class ProductList extends Component {
    constructor (props) {
        super(props);
        this.state = {
            search: '',
            modalVisible: false,
            products: [],
            allProducts: [],
            isLoading: true,
            isVisible: false,
            itemClicked: null,
        }
    }

    componentDidMount () {
        let db = SQLite.openDatabase(
            {
                name: "CRMDatabase.db",
                createFromLocation: "~database.db",
                location: "Library"
            },
            this.openCB, 
            this.errorCB,
        );
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM Products', [], (tx, result) => {
                console.log("Query completed");
                var data = [];
                var len = result.rows.length;
                console.log("Len:" + len);
                for (let i = 0; i < len; i++) {
                    let row = result.rows.item(i);
                    data.push(row);
                }
                this.setState({
                    products: data,
                    allProducts: data,
                    isLoading: false,
                })
            })
        });
    }

    filterItems = (search) => {
        const filteredData = this.state.allProducts.filter(item => item.Name.includes(search));
        this.setState({ products: filteredData })
        if (search == "") {
            this.setState({ products: this.state.allProducts })
        }
    }

    renderActivity = (Activity) => {
        if (Activity == 1) {
            return (<Text style = {{color: 'green', alignSelf: 'center'}}>Active</Text>);
        } else {
            return (<Text style = {{color: 'red', alignSelf: 'center'}}>Not Active</Text>)
        }
    }

    returnData = (newProduct) => {
        let newproduct = this.state.allProducts.concat(newProduct);
        this.setState({
            allProducts: newproduct,
            products: newproduct,
        });
    }

    returnUpdatedData = (updatedProduct) => {
        let stableProducts = this.state.allProducts.filter(item => item.ID != updatedProduct.ID);
        let updatedProducts = stableProducts.concat(updatedProduct);
        this.setState({
            allProducts: updatedProducts,
            products: updatedProducts,
        });
    }

    renderItem = ({item, index}) => {
        if (this.state.isVisible == true && index == this.state.itemClicked) {
            return (
                <View style = {[styles.productsItem, {backgroundColor: 'rgba(243, 226, 179, 0.7)'}]}>
                    <View style = {{flexDirection: 'row', flex: 1}}>
                        <TouchableOpacity style = {styles.items} onPress = {() => {
                            this.props.navigation.navigate("ProductDetails", {product: item})
                            this.setState({isVisible: false})
                            }}>
                            <View style = {styles.icons}>
                                <Icon name = "address-card" size = {30} color = {'#2aa1b7'} />
                                <Text style = {{color: '#2aa1b7'}}>Details</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.items} onPress = {() => {
                            this.props.navigation.navigate("editProduct", {product: item, returnUpdatedData: this.returnUpdatedData.bind(this)});
                            this.setState({isVisible: false})
                        }}>
                            <View style = {styles.icons}>
                                <Icon name = "pencil-square-o" size = {30} color = {'#2aa1b7'}/>
                                <Text style = {{color: '#2aa1b7'}}>Edit</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style = {{flexDirection: 'row', flex: 1}}>
                    <TouchableOpacity style = {styles.items} onPress = {() => {
                        // this.setState({modalVisible: true})
                        this.props.navigation.navigate("ProductGallery", {product: item})
                    }}>
                            <View style = {styles.icons}>
                                <Icon name = "image" size = {30} color = {'#2aa1b7'} />
                                <Text style = {{color: '#2aa1b7'}}>Gallery</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.items} onPress = {() => this.setState({isVisible: false})}>
                            <View style = {styles.icons}>
                                <Icon name = "close" size = {30} color = {'#2aa1b7'} />
                                <Text style = {{color: '#2aa1b7'}}>Close</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
        return (
            <TouchableOpacity
                style = {[styles.productsItem, {backgroundColor: '#fff'}]}
                onPress = {() => {this.setState({
                            isVisible: !this.state.isVisible,
                            itemClicked: index
                        });
                    }
                }
                onLongPress = {() => {
                    Alert.alert(
                        'Delete Product',
                        'Are you sure to delete this product?',
                        [
                            {text: 'NO', style: 'cancel'},
                            {text: 'YES', onPress: () => 
                                {
                                    db.transaction(tx => {
                                        tx.executeSql('DELETE FROM Products WHERE ID=?', [item.ID], (tx, result) => {
                                            console.log(item.ID)
                                            console.log("Product Deleted");
                                        })
                                    });
                                    {this.deleteItemByID(item.ID)}
                                }
                            },
                        ]
                    );
                }}>
                    <Avatar
                        rounded
                        source = {{uri: item.Image}}
                        title = {item.Name.slice(0,2).toUpperCase()}
                        containerStyle = {styles.avatar}
                        size = {60}/>
                    <View>
                        <Text style = {{fontSize: 16, fontWeight: 'bold', alignSelf: 'center'}}>
                            {(item.Name.length > 15) ? item.Name.slice(0,15) + "..." : item.Name}
                        </Text>
                        <Text style = {{color: '#777', alignSelf: 'center'}}>{item.Price + " $"}</Text>
                        {this.renderActivity(item.Activity)}
                    </View>
            </TouchableOpacity>
        );

    }

    deleteItemByID = (id) => {
        const filteredData = this.state.products.filter(item => item.ID != id);
        this.setState({ products: filteredData });
    }

    render () {
        if (this.state.isLoading) {
            return (
                <View style = {[styles.container, {alignItems: 'center', justifyContent: 'center'}]}>
                      <Bars size={20} color="#000" />
                </View>
            )
        }
        if (this.state.allProducts.length === 0) {
            return (
                <View style = {styles.container}>
                    <SearchBar
                        containerStyle = {styles.searchBar}
                        inputContainerStyle = {styles.inputSearchBar}
                        inputStyle = {styles.inputSearch}
                        searchIcon = {<Icon name = 'search' size = {20} />}
                        placeholder="Search ..."
                        onChangeText={search => {this.setState({ search })}}
                        value={this.state.search}
                    />
                    <View style = {styles.notFound}>
                        <View style = {styles.notFoundContainer}>
                            <Text style = {{fontSize: 18, fontWeight: 'bold', color: '#f03613'}}>Not Found!</Text>
                        </View>
                    </View> 
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{ }}
                        style={{ backgroundColor: '#27b46e' }}
                        position="bottomRight"
                        onPress = {() => this.props.navigation.navigate("AddProduct", {returnData: this.returnData.bind(this)})}>
                            <Icon name="plus" />
                    </Fab>               
                </View>
            )
        }
        return (
            <View style = {styles.container}>
                <SearchBar
                    containerStyle = {styles.searchBar}
                    inputContainerStyle = {styles.inputSearchBar}
                    inputStyle = {styles.inputSearch}
                    searchIcon = {<Icon name = 'search' size = {20} />}
                    placeholder="Search ..."
                    onChangeText={search => {
                        this.setState({ search })
                        {this.filterItems(search)}
                    }}
                    value={this.state.search}
                />
                <View style = {{marginTop: 5, marginHorizontal: 5, marginBottom: 115}}>
                    <FlatList
                        numColumns = {2}
                        data = {this.state.products}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {this.renderItem}
                    />
                </View>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#27b46e' }}
                    position="bottomRight"
                    onPress = {() => this.props.navigation.navigate("AddProduct", {returnData: this.returnData.bind(this)})}>
                        <Icon name="plus" />
                </Fab>       
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    addLeadBtn: {
        borderBottomColor: '#05a3a4',
        marginHorizontal: 10,
        paddingVertical: 5,
        justifyContent: 'center',
        borderBottomWidth: 2,
    },
    addLeadTxt: {
        fontSize: 18,
        color: '#05a3a4', 
        fontWeight: 'bold'
    },
    searchBar: {
        backgroundColor: 'transparent',
        borderBottomWidth: 0,
        borderTopWidth: 0,
    },
    inputSearchBar: {
        backgroundColor : '#fff',
        paddingHorizontal: 5,
        borderWidth:2,
        borderBottomWidth: 2,
        borderRadius: 50,
        borderColor: '#fcce01'
    },
    inputSearch: {
        fontSize: 18
    },
    productsItem: {
        flex: 1,
        height: 130,
        alignItems: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        padding: 5,
        borderRadius: 2,
        justifyContent: 'center',
        elevation: 5,
    },
    avatar: {
        justifyContent: 'center',
    },
    notFound: {
        width: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1,
    },
    notFoundContainer: {
        width: 150, 
        height: 150, 
        borderWidth: 2, 
        alignSelf: 'center', 
        borderRadius: 75, 
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#f03613'
    },
    items: {
        flex: 1,
        margin: 3,
        padding: 5,
        borderRadius: 3,
        borderWidth: 1,
        backgroundColor: '#fff',
        borderColor: '#2aa1b7'
    },
    icons: {
        justifyContent: 'center',
        alignItems: 'center',
    }
})  