import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity, Image } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Switch } from 'react-native-paper';
import ImagePicker from 'react-native-image-crop-picker';
import SQLite from 'react-native-sqlite-storage';
let db = null;

export default class AddProduct extends Component {
    
    static navigationOptions = ({navigation}) => ({
        title : 'Add Product',
        headerStyle: {
            backgroundColor: '#1d7d9e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })
    
    constructor (props) {
        super(props);
        this.state = {
            Activity : true,
            nameFocus: false,
            codeFocus: false,
            priceFocus: false,
            descFocus: false,
            Name: "",
            Id: "",
            Activity: "",
            Price: "",
            Desc: "",
            Image: "",
        }
    }

    add = () => {
        let product = {
            Name: this.state.Name,
            Id: this.state.Id,
            Activity: this.state.Activity,
            Price: this.state.Price,
            Desc: this.state.Desc,
            Image: this.state.Image
        }
        if (product.Name.length == 0) {
            alert("Name is required");
            this._name.focus();
        } else if (product.Id.length == 0) {
            this._code.focus();
            alert("Code is required")
        } else if (product.Price.length == 0) {
            this._price.focus();
            alert("Price is required")
        } else {
            db = SQLite.openDatabase(
                {
                    name: "CRMDatabase.db",
                    createFromLocation: "~database.db",
                    location: "Library"
                },
                this.openCB, 
                this.errorCB,
            );
            
            db.transaction(tx => {
                tx.executeSql(
                    'INSERT INTO Products (Code, Name, Activity, Price, Desc, Image) VALUES (?, ?, ?, ?, ?, ?)',
                    [product.Id, product.Name, product.Activity, product.Price, product.Desc, product.Image]
                )
                console.log("Product Added.")
            });
            this.props.navigation.state.params.returnData(product);
            this.props.navigation.goBack();
        }
    }

    render () {
        return (
            <View style = {styles.container}>
                <View style = {{alignSelf: 'center', marginTop: 10}}>
                    <Avatar
                        rounded
                        showEditButton
                        onEditPress = {() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true
                              }).then(image => {
                                console.log(image);
                                this.setState({
                                    Image: image.path
                                })
                                console.log(this.state.avatarSource)
                            });
                        }}
                        source = {(this.state.Image) ? {uri: this.state.Image} : null}
                        size = {100}
                    />
                </View>
                <ScrollView style = {{ margin: 10}}>
                    <Text style = {styles.textNecessary}>Name</Text>
                    <TextInput
                        ref = {(input) => { this._name = input; }}
                        onChangeText={(text) => this.setState({Name: text})}
                        value={this.state.Name}
                        onFocus = {() => { this.setState({nameFocus: true}) }} 
                        onBlur = {() => { this.setState({nameFocus: false}) }}
                        placeholder = {"Example: Washing Machine"}
                        style = {(this.state.nameFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.textNecessary}>Code (ID)</Text>
                    <TextInput
                        ref = {(input) => { this._code = input; }}
                        onChangeText={(text) => this.setState({Id: text})}
                        value={this.state.Id}
                        keyboardType = "number-pad"
                        onFocus = {() => { this.setState({codeFocus: true}) }} 
                        onBlur = {() => { this.setState({codeFocus: false}) }}
                        placeholder = {"Example: 101"}
                        style = {(this.state.codeFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.textNecessary}>Price</Text>
                    <TextInput
                        ref = {(input) => { this._price = input; }}
                        onChangeText={(text) => this.setState({Price: text})}
                        keyboardType = "number-pad"
                        value={this.state.Price}
                        onFocus = {() => { this.setState({priceFocus: true}) }} 
                        onBlur = {() => { this.setState({priceFocus: false}) }}
                        placeholder = {"Example: 100"}
                        style = {(this.state.priceFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.text}>Description</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Desc: text})}
                        value={this.state.Desc}
                        onFocus = {() => { this.setState({descFocus: true}) }}
                        onBlur = {() => { this.setState({descFocus: false}) }}
                        numberOfLines = {5}
                        placeholder = {"Type here ..."}
                        style = {(this.state.descFocus) ? styles.activeInput : styles.inactiveInput}/>
                    <View style = {styles.activeSwitch}>
                        <Text style = {styles.textActive}>Product Active</Text>
                        <Switch
                            style = {{alignSelf: 'center', position: 'absolute', right: 10}}
                            value = {this.state.Activity}
                            onValueChange={() => { this.setState({ Activity: !this.state.Activity }); }}
                        />
                    </View>
                </ScrollView>
                <View style = {{alignItems: 'center', justifyContent: 'center', marginBottom: 10}}>
                    <TouchableOpacity style = {styles.submitBtn} onPress = {() => { this.add() }}>
                        <Text style ={{fontWeight: 'bold', fontSize: 18, color: '#fff'}}>Submit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    confirmBtn: {
        marginRight: 5,
    },
    text: {
        color: '#05a3a4',
        marginTop: 5,
        fontSize: 18,
        alignSelf: "center",
        fontWeight: 'bold'
    },
    textNecessary: {
        color: '#f03613',
        marginTop: 5,
        fontSize: 18,
        alignSelf: "center",
        fontWeight: 'bold'
    },
    inactiveInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    activeInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#88bece'
    },
    activeNecessaryInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#F08EA7'
    },
    activeSwitch: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 8,
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
    },
    textActive: {
        color: '#f03613',
        fontSize: 18,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    submitBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000', 
        paddingVertical: 10,
        paddingHorizontal: 40,
        borderRadius: 10, 
        backgroundColor: '#27b46e',
    }
})  