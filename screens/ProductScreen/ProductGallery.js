import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, Alert, Dimensions, Modal} from 'react-native';
import {Fab} from 'native-base'
import SQLite from 'react-native-sqlite-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import { Avatar } from 'react-native-elements';


var db = SQLite.openDatabase({
    name: "CRMDatabase.db",
    createFromLocation: "~database.db",
    location: "Library"
})

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class ProductGallery extends Component {

    static navigationOptions = ({navigation}) => ({
        title : 'Product Gallery',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
    })

    constructor (props) {
        super(props)
        this.state = {
            images: [],
            product: this.props.navigation.state.params.product,
            modalVisible: false,
            imageIndex: 0
        }
    }

    componentDidMount () {
        db.transaction(tx => {
            tx.executeSql('SELECT Image FROM ProductImage WHERE ProductID = ? ', [this.state.product.ID], (tx, result) => {
                console.log("Query completed");
                var data = [];
                var len = result.rows.length;
                console.log("Len:" + len);
                for (let i = 0; i < len; i++) {
                    let row = result.rows.item(i);
                    data.push(row);
                }
                // console.log(data)
                this.setState({
                    images: data,
                    isLoading: false,
                })
            })
        });
    }

    render () {
        return (
            <View style = {[styles.container, {alignItems: 'center', justifyContent: 'center'}]}>
                <View style = {{flex: 1, marginTop: 5, marginHorizontal: 5, marginBottom: 5}}>
                    <FlatList 
                        numColumns = {3}
                        data = {this.state.images}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {({item, index}) => 
                            <View style = {{
                                marginHorizontal: 5,
                                marginBottom: 5,
                                padding: 5,
                            }}>
                                <Avatar
                                    // rounded
                                    onPress = {() => {
                                        this.setState({
                                            modalVisible: !this.state.modalVisible,
                                            imageIndex: index
                                        })
                                    }}
                                    source = {{uri: item.Image}}
                                    title = {this.state.product.Name.slice(0,2).toUpperCase()}
                                    size = {screenWidth / 4}
                                    avatarStyle = {{borderWidth: 2, borderTopWidth: 2, borderColor: '#dab238'}}
                                />
                                <Modal
                                    animationType = {"fade"}
                                    // transparent={true}
                                    visible={this.state.modalVisible}>
                                        <View style = {{
                                            alignItems: "center",
                                            justifyContent: 'center',
                                            backgroundColor: '#444',
                                            flex: 1,
                                            padding: 5
                                        }}>
                                            <TouchableOpacity
                                                style = {{
                                                    position: 'absolute',
                                                    top: 20,
                                                    left: 20
                                                }}
                                                onPress = {() => this.setState({modalVisible: !this.state.modalVisible})}
                                            >
                                                <View>
                                                    <Icon name = "arrow-left" size = {20} color = {'white'}/>
                                                </View>
                                            </TouchableOpacity>
                                            <Image
                                                // rounded
                                                // size s= {300} 
                                                style={{width: screenWidth - 20, height: 400, alignSelf: 'center'}}
                                                source = {{uri: this.state.images[this.state.imageIndex].Image}}
                                            />
                                        </View>

                                </Modal>
                            </View>
                        }
                    />
                </View>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#27b46e' }}
                    position="bottomRight"
                    onPress={() => {
                        ImagePicker.openPicker({
                            width: 300,
                            height: 400,
                            cropping: true,
                            multiple: true,
                        }).then(image => {
                            // console.log(image);
                            var newImage = this.state.images
                            for (let i = 0; i < image.length; i++) {
                                newImage = newImage.concat({Image: image[i].path})
                            }
                            console.log('........................................................')
                            console.log(newImage)
                            this.setState({
                                images: newImage
                            })
                            for (let i = 0; i < image.length; i++) {
                                db.transaction(tx => {
                                    tx.executeSql(
                                        'INSERT INTO ProductImage (ProductID, Image) VALUES (?, ?)',
                                        [this.state.product.ID, image[i].path]
                                    )
                                    console.log("Product Added.")
                                });
                            }
                        });
                    }}>
                        <Icon name="plus" />
                </Fab>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff'
    },
})