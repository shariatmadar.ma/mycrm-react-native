import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity, Image } from 'react-native';
import { Switch } from 'react-native-paper';
import SQLite from 'react-native-sqlite-storage';
import { Avatar } from 'react-native-elements';
let db = null;

export default class editProduct extends Component {
    
    static navigationOptions = ({navigation}) => ({
        title : 'Edit Product',
        headerStyle: {
            backgroundColor: '#1d7d9e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })
    
    constructor (props) {
        super(props);
        this.state = {
            ID: this.props.navigation.state.params.product.ID,
            Activity : this.props.navigation.state.params.product.Activity,
            Name: this.props.navigation.state.params.product.Name,
            Code: this.props.navigation.state.params.product.Code,
            Activity: this.props.navigation.state.params.product.Activity,
            Price: this.props.navigation.state.params.product.Price,
            Desc: this.props.navigation.state.params.product.Desc,
            Image: this.props.navigation.state.params.product.Image,
            nameFocus: false,
            codeFocus: false,
            priceFocus: false,
            descFocus: false,
        }
    }

    componentDidMount () {
    }

    update = () => {
        db = SQLite.openDatabase(
            {
                name: "CRMDatabase.db",
                createFromLocation: "~database.db",
                location: "Library"
            },
            this.openCB, 
            this.errorCB,
        );
        let product = {
            ID: this.state.ID,
            Name: this.state.Name,
            Code: this.state.Code,
            Activity: this.state.Activity,
            Price: this.state.Price,
            Desc: this.state.Desc,
        }
        db.transaction(tx => {
            tx.executeSql(
                'UPDATE Products SET Code = ?, Name = ?, Activity = ?, Price = ?, Desc = ? WHERE ID = ?',
                [product.Code, product.Name, product.Activity , product.Price, product.Desc, product.ID]
            );
            console.log("Product Updated.")
        });
        this.props.navigation.state.params.returnUpdatedData(product);
        this.props.navigation.goBack();
    }

    render () {
        return (
            <View style = {styles.container}>
                <View style = {{alignSelf: 'center', marginTop: 10}}>
                    <Avatar
                        rounded
                        showEditButton
                        onEditPress = {() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true
                              }).then(image => {
                                console.log(image);
                                this.setState({
                                    Image: image.path
                                })
                                console.log(this.state.avatarSource)
                            });
                        }}
                        source = {{uri: this.state.Image}}
                        containerStyle = {styles.avatar}
                        size = {100}
                    />
                </View>
                <ScrollView style = {{margin: 10}}>
                    <Text style = {styles.textNecessary}>Name</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Name: text})}
                        value={this.state.Name}
                        onFocus = {() => { this.setState({nameFocus: true}) }}
                        onBlur = {() => { this.setState({nameFocus: false}) }}
                        placeholder = {"Example: Washing Machine"}
                        style = {(this.state.nameFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.textNecessary}>Code (ID)</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Code: text})}
                        value={this.state.Code.toString()}
                        onFocus = {() => { this.setState({codeFocus: true}) }} 
                        onBlur = {() => { this.setState({codeFocus: false}) }}
                        placeholder = {"Example: 101"}
                        style = {(this.state.codeFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.textNecessary}>Price</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Price: text})}
                        value={this.state.Price.toString()}
                        onFocus = {() => { this.setState({priceFocus: true}) }} 
                        onBlur = {() => { this.setState({priceFocus: false}) }}
                        placeholder = {"Example: 100"}
                        style = {(this.state.priceFocus) ? styles.activeNecessaryInput : styles.inactiveInput}/>
                    <Text style = {styles.text}>Description</Text>
                    <TextInput
                        onChangeText={(text) => this.setState({Desc: text})}
                        value={this.state.Desc}
                        onFocus = {() => { this.setState({descFocus: true}) }}
                        onBlur = {() => { this.setState({descFocus: false}) }}
                        numberOfLines = {5}
                        placeholder = {"Type here ..."}
                        style = {(this.state.descFocus) ? styles.activeInput : styles.inactiveInput}/>
                    <View style = {styles.activeSwitch}>
                        <Text style = {styles.textActive}>Product Active</Text>
                        <Switch
                            style = {{alignSelf: 'center', position: 'absolute', right: 10}}
                            value = {this.state.Activity}
                            onValueChange={() => { this.setState({ Activity: !this.state.Activity }); }}
                        />
                    </View>
                </ScrollView>
                <View style = {{alignItems: 'center', justifyContent: 'center', marginBottom: 10}}>
                    <TouchableOpacity style = {styles.submitBtn} onPress = {() => { this.update(); }}>
                        <Text style ={{fontWeight: 'bold', fontSize: 18, color: '#fff'}}>Submit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    confirmBtn: {
        marginRight: 5,
    },
    text: {
        color: '#05a3a4',
        marginTop: 5,
        fontSize: 18,
        alignSelf: "center",
        fontWeight: 'bold'
    },
    textNecessary: {
        color: '#f03613',
        marginTop: 5,
        fontSize: 18,
        alignSelf: "center",
        fontWeight: 'bold'
    },
    inactiveInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    activeInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#88bece'
    },
    activeNecessaryInput: {
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 3,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#F08EA7'
    },
    activeSwitch: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        fontSize: 16,
        alignSelf: 'center',
        width: '100%',
        marginVertical: 8,
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
    },
    textActive: {
        color: '#f03613',
        fontSize: 18,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    submitBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000', 
        paddingVertical: 10,
        paddingHorizontal: 40,
        borderRadius: 10, 
        backgroundColor: '#27b46e',
        marginTop: 15
    }
})  