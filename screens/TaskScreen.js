import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import TaskList from './TaskScreen/TaskList';
import TaskDetails from './TaskScreen/TaskDetails';
import AddTask from './TaskScreen/AddTask';

const MainNavigator = createStackNavigator({
    TaskList: {
        screen: TaskList,
        navigationOptions: ({navigation}) => ({
            title : 'Tasks',
            headerStyle: {
                backgroundColor: '#1d7d9e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        })
    },
    TaskDetails: {
        screen: TaskDetails,
    },
    AddTask: {
        screen: AddTask,
    },
});

export default createAppContainer(MainNavigator);