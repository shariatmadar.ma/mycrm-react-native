import React, {Component} from 'react';
import { View, StyleSheet, Text, TextInput, ScrollView} from 'react-native';
import { Avatar } from 'react-native-elements';
import ImagePicker from 'react-native-image-crop-picker';
import AsyncStorage from '@react-native-community/async-storage';
import {Fab} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';


export default class OwnerProfile extends Component {

    static navigationOptions = ({navigation}) => ({
        title : 'Owner',
        headerStyle: {
            backgroundColor: '#1d7d9e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    })

    constructor (props) {
        super(props);
        this.state = {
            pic: '',
            name: '',
            family: '',
            job: '',
            company: '',
            phone: '',
            mobile: '',
            email: '',
        };
    }

    _saveUser = async() => {
        try {
            await AsyncStorage.setItem('userToken', 'yes')
            await AsyncStorage.setItem('pic', this.state.pic)
            await AsyncStorage.setItem('name', this.state.name)
            await AsyncStorage.setItem('family', this.state.family)
            await AsyncStorage.setItem('mobile', this.state.mobile)
            await AsyncStorage.setItem('job', this.state.job)
            await AsyncStorage.setItem('phone', this.state.phone)
            await AsyncStorage.setItem('email', this.state.email)
            await AsyncStorage.setItem('company', this.state.company)
        } catch (e) {
            console.log(e)
        }
        this.props.navigation.navigate("Dashboard")
    }

    _userData = async() => {
        const name = await AsyncStorage.getItem('name')
        const family = await AsyncStorage.getItem('family')
        const job = await AsyncStorage.getItem('job')
        console.log("job", job)
        const mobile = await AsyncStorage.getItem('mobile')
        const pic = await AsyncStorage.getItem('pic')
        const phone = await AsyncStorage.getItem('phone')
        const email = await AsyncStorage.getItem('email')
        const company = await AsyncStorage.getItem('company')
        this.setState({
            name,
            family,
            job,
            mobile,
            pic,
            phone,
            email,
            company
        }, () => {
            console.log(this.state)
        })
    }

    componentDidMount() {
        this._userData()   
    }

    render () {
        return (
            <View style = {styles.container}>
                <ScrollView>
                    <View style = {{width: '35%' , height: '100%', position: 'absolute', backgroundColor: '#47c7eb'}}></View>
                    <View style ={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                        <View style = {{width: '35%'}}><Text style = {styles.text}>Picture</Text></View>
                        <View style = {{width: '65%', alignItems: 'center'}}>
                            <Avatar 
                                rounded
                                size = {100}
                                source = {(this.state.pic) ? {uri: this.state.pic} : null} 
                                containerStyle = {{borderWidth: 3, borderColor: '#dab238'}}
                                showEditButton
                                onEditPress = {() => {
                                    ImagePicker.openPicker({
                                        width: 300,
                                        height: 400,
                                        cropping: true
                                      }).then(image => {
                                        console.log(image);
                                        this.setState({
                                            pic: image.path
                                        })
                                        console.log(this.state.avatarSource)
                                    });
                                }}
                            />
                        </View>
                    </View>
                    <View style ={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                        <View style = {{width: '35%',}}><Text style = {styles.text}>Name</Text></View>
                        <TextInput
                            onChangeText={(text) => this.setState({name: text})}
                            value={this.state.name} 
                            style = {styles.input}
                            placeholder = {"Type Here ..."}
                        />
                    </View>
                    <View style ={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                        <View style = {{width: '35%',}}><Text style = {styles.text}>Family</Text></View>
                        <TextInput 
                            onChangeText={(text) => this.setState({family: text})}
                            value={this.state.family}
                            style = {styles.input}
                            placeholder = {"Type Here ..."}
                        />
                    </View>
                    <View style ={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                        <View style = {{width: '35%',}}><Text style = {styles.text}>Job</Text></View>
                        <TextInput
                            onChangeText={(text) => this.setState({job: text})}
                            value={this.state.job} 
                            style = {styles.input}
                            placeholder = {"Type Here ..."}
                        />
                    </View>
                    <View style ={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                        <View style = {{width: '35%',}}><Text style = {styles.text}>Company</Text></View>
                        <TextInput
                            onChangeText={(text) => this.setState({company: text})}
                            value={this.state.company} 
                            style = {styles.input}
                            placeholder = {"Type Here ..."}
                        />
                    </View>
                    <View style ={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                        <View style = {{width: '35%',}}><Text style = {styles.text}>Phone</Text></View>
                        <TextInput
                            onChangeText={(text) => this.setState({phone: text})}
                            keyboardType = "phone-pad"
                            value={this.state.phone} 
                            style = {styles.input}
                            placeholder = {"Type Here ..."}
                        />
                    </View>
                    <View style ={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                        <View style = {{width: '35%'}}><Text style = {styles.text}>Mobile</Text></View>
                        <TextInput
                            onChangeText={(text) => this.setState({mobile: text})}
                            value={this.state.mobile} 
                            keyboardType = "phone-pad"
                            style = {styles.input}
                            placeholder = {"Type Here ..."}
                        />
                    </View>
                    <View style ={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                        <View style = {{width: '35%'}}><Text style = {styles.text}>Email</Text></View>
                        <TextInput
                            onChangeText={(text) => this.setState({email: text})}
                            value={this.state.email} 
                            style = {styles.input}
                            keyboardType = "email-address"
                            placeholder = {"Type Here ..."}
                        />
                    </View>
                </ScrollView>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#27b46e' }}
                    position="bottomRight"
                    onPress={this._saveUser}>
                        <Icon name="check" />
                </Fab>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f9ff',
    },
    text: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 20,
        zIndex: 2,
        color: '#2d3258',
    },
    input: {
        paddingHorizontal: 5,
        fontSize: 16,
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#dab238',
        zIndex: 2
    }
})