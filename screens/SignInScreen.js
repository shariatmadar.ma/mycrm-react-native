import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput, Dimensions } from 'react-native'
import { Avatar } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class SignInScreen extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            pic: '',
            name: '',
            family: '',
            mobile: '',
            job: ''
        }
    }

    static navigationOptions = ({navigation}) => ({
        title : null,
        headerTransparent: true,
        headerStyle: {
        },
        headerTintColor: '#fff',
    })

    _signInAsync = async() => {
        console.log(this.state)
        await AsyncStorage.setItem('userToken', 'yes');
        try {
            await AsyncStorage.setItem('pic', this.state.pic);
            await AsyncStorage.setItem('name', this.state.name);
            await AsyncStorage.setItem('family', this.state.family);
            await AsyncStorage.setItem('mobile', this.state.mobile);
            await AsyncStorage.setItem('job', this.state.job);
        }
        catch (e) {
            console.log(e)
        }
        this.props.navigation.navigate('App')
    }
    
    render() {
        return (
            <View style = {styles.container}>
                <Avatar 
                    rounded
                    size = {100}
                    source = {(this.state.pic) ? {uri: this.state.pic} : null}
                    containerStyle = {{marginBottom: 10, borderWidth: 3, borderColor: '#dab238'}}
                    showEditButton
                    onEditPress = {() => {
                        ImagePicker.openPicker({
                            width: 300,
                            height: 400,
                            cropping: true
                          }).then(image => {
                            console.log(image);
                            this.setState({
                                pic: image.path
                            })
                            console.log(this.state.avatarSource)
                        });
                    }}
                />
                <TextInput
                    onChangeText={(text) => this.setState({name: text})}
                    value={this.state.name}
                    placeholder = "Enter youre Name"
                    style = {{
                        padding: 10,
                        backgroundColor: '#fff',
                        width: screenWidth - 20,
                        marginHorizontal: 10,
                        marginVertical: 5,
                        textAlign: 'center',
                        elevation: 2,
                        borderRadius: 10
                    }}
                />
                <TextInput
                    onChangeText={(text) => this.setState({family: text})}
                    value={this.state.family}
                    placeholder = "Enter youre Family"
                    style = {{
                        padding: 10,
                        backgroundColor: '#fff',
                        width: screenWidth - 20,
                        marginHorizontal: 10,
                        marginVertical: 5,
                        textAlign: 'center',
                        elevation: 2,
                        borderRadius: 10
                    }}
                />
                <TextInput
                    onChangeText={(text) => this.setState({mobile: text})}
                    value={this.state.mobile}
                    keyboardType = "phone-pad"
                    placeholder = "Enter youre Mobile"
                    style = {{
                        padding: 10,
                        backgroundColor: '#fff',
                        width: screenWidth - 20,
                        marginHorizontal: 10,
                        marginVertical: 5,
                        textAlign: 'center',
                        elevation: 2,
                        borderRadius: 10
                    }}
                />
                <TextInput
                    onChangeText={(text) => this.setState({job: text})}
                    value={this.state.job}
                    placeholder = "Enter youre Job"
                    style = {{
                        padding: 10,
                        backgroundColor: '#fff',
                        width: screenWidth - 20,
                        marginHorizontal: 10,
                        marginVertical: 5,
                        textAlign: 'center',
                        elevation: 2,
                        borderRadius: 10
                    }}
                />
                <TouchableOpacity
                    onPress = {this._signInAsync}
                >
                    <View
                    style = {{
                        paddingHorizontal: 20,
                        backgroundColor: '#27b46e',
                        paddingVertical: 10,
                        borderRadius: 10,
                        marginTop: 10,
                        width: 150,
                        alignItems: 'center'
                    }}>
                        <Text style = {{
                            fontSize: 18,
                            color: '#fff',
                            fontWeight: 'bold'
                        }}>
                            Sign In
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor: '#e6f9ff',
      alignItems: 'center',
      justifyContent: 'center' 
    },
});