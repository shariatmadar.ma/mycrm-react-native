import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import OwnerProfile from './OwnerProfile';
import Dashboard from './Dashboard';

const MainNavigator = createStackNavigator({
    Dashboard: {
        screen: Dashboard,
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
        }
    },
    OwnerProfile: {
        screen: OwnerProfile,
    }
});

export default createAppContainer(MainNavigator);