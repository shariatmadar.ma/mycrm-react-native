import React from 'react'

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';

import HomeScreen from './screens/HomeScreen';
import ProductScreen from './screens/ProductScreen';
import LeadScreen from './screens/LeadScreen';
import TaskScreen from './screens/TaskScreen';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import SignInScreen from './screens/SignInScreen';

const AuthStack = createStackNavigator(
    { 
        SignIn: SignInScreen 
    }
);

const TabNavigator = createMaterialBottomTabNavigator(
    {
        Dashboard: {
            screen: HomeScreen,
            navigationOptions: () => ({
                tabBarIcon: ({ focused, horizontal, tintColor }) => {
                    let color = '#fff'
                    focused ? color = '#fcce01' : color = '#fff';
                    return (<Icon name="id-card" size={20} color = {color}/>)
                },
            })
        },
        Products: {
            screen: ProductScreen,
            navigationOptions: () => ({
                tabBarIcon: ({ focused, horizontal, tintColor }) => {
                    let color = '#fff'
                    focused ? color = '#fcce01' : color = '#fff';
                    return (<Icon name="archive" size={20} color = {color}/>)
                }
            })
        },
        Leads: {
            screen: LeadScreen,
            navigationOptions: () => ({
                tabBarIcon: ({ focused, horizontal, tintColor }) => {
                    let color = '#fff'
                    focused ? color = '#fcce01' : color = '#fff';
                    return (<Icon name="users" size={20} color = {color}/>)
                }
            })
        },
        Tasks: {
            screen: TaskScreen,
            navigationOptions: () => ({
                tabBarIcon: ({ focused, horizontal, tintColor }) => {
                    let color = '#fff'
                    focused ? color = '#fcce01' : color = '#fff';
                    return (<Icon name="tasks" size={20} color = {color}/>)
                }
            })
        },
    },
    {
        initialRouteName: 'Dashboard',
        activeColor: '#fcce01',
        barStyle: { backgroundColor: '#1d7d9e' },
    }
)

export default createAppContainer(
    createSwitchNavigator(
        {
            AuthLoading: AuthLoadingScreen,
            App: TabNavigator,
            // App: AuthLoadingScreen,
            // App: AuthStack,
            Auth: AuthStack
        },
        {
            initialRouteName: 'AuthLoading'
        }
    )
);